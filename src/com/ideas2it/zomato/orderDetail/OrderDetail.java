package com.ideas2it.zomato.orderDetail;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * This is model class for Cart Detail .
 */
public class OrderDetail {

    private int id;
    private Order order;
    private String menuItemName;
    private int quantity;
    private float price;
    
    public OrderDetail() {
    
    }
    
    public OrderDetail(String menuItemName, int quantity, float price) {
        this.menuItemName = menuItemName;
        this.quantity = quantity;
        this.price = price;
    }
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }
    
    public Order getOrder() {
        return order;
    }
    
    public void setMenuItemName(String menuItemName) {
        this.menuItemName = menuItemName;
    }
    
    public String getMenuItemName() {
        return menuItemName;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public float getPrice() {
        return price;
    }
    
    public float getTotalPrice() {
        return price * quantity;
    }
}
