package com.ideas2it.zomato.role.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.dao.RoleDao;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Performs all the database related funcionalities for role.
 */
@Repository ("roleDao")
public class RoleDaoImpl implements RoleDao {

    private SessionFactory sessionFactory;

    /**
     * Inserts a new role into the database.
     *
     * @param role
     *            - role object about to be inserted.
     *
     * @throws ZomatoException when inserting role fails.
     */
    public void insert(Role role) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        session.save(role); 
    }
    
    /**
     * Deletes the role from the database specified by id.
     *
     * @param id
     *            - id of role to be deleted.
     *
     * @throws ZomatoException when deleting role fails.
     */
    public void delete(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Role role = (Role) session.get(Role.class, id);
        session.delete(role);
    }
    
    /**
     * Returns all roles in database.
     *
     * @return user roles.
     *
     * @throws ZomatoException if getting roles fails.
     */
    public Set getRoles() throws ZomatoException {
        int adminId = 1; 
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Role.class);
        criteria.add(Restrictions.ne("id", adminId));
        return new HashSet(criteria.list());
    }

    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role .
     *
     * @return role specified by id.
     *
     * @throws ZomatoException when getting role by id fails.
     */
    public Role getRoleById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Role.class);
        criteria.add(Restrictions.eq("id", id));
        return (Role)criteria.uniqueResult();
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
