package com.ideas2it.zomato.role.dao;

import java.util.Set;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Role dao class
 */
public interface RoleDao {

    /**
     * Inserts a new role into the database.
     *
     * @param role
     *            - role object about to be inserted.
     *
     * @throws ZomatoException when inserting role fails.
     */
    void insert(Role role) throws ZomatoException;
    
    /**
     * Deletes the role from the database specified by id.
     *
     * @param id
     *            - id of role to be deleted.
     *
     * @throws ZomatoException when deleting role fails.
     */
    void delete(int id) throws ZomatoException;
    
    /**
     * Returns all roles in database.
     *
     * @return user roles.
     *
     * @throws ZomatoException if getting roles fails.
     */
    Set getRoles() throws ZomatoException;
    
    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role .
     *
     * @return role specified by id.
     *
     * @throws ZomatoException when getting role by id fails.
     */
    Role getRoleById(int id) throws ZomatoException;
}
