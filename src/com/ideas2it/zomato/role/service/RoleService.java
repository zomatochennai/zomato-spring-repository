package com.ideas2it.zomato.role.service;

import java.util.Set;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for role service class.
 */
public interface RoleService {

    /**
     * Adds a new role to the database.
     *
     * @param roleName
     *            - name of role to be added .
     *
     * @throws ZomatoException when adding role fails.
     */
    void addRole(String roleName) throws ZomatoException;
    
    /**
     * Deletes the role from database.
     *
     * @param roleId
     *            - id of role to be deleted .
     *
     * @throws ZomatoException when deleting role fails.
     */
    void deleteRole(int roleId) throws ZomatoException;
    
    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role .
     *
     * @return role specified by id.
     *
     * @throws ZomatoException when getting role fails.
     */
    Role getRoleById(int id) throws ZomatoException;
    
    /**
     * Returns all the user role from database.
     *
     * @return set of roles.
     *
     * @throws ZomatoException when getting roles fail.
     */
    Set getRoles() throws ZomatoException;
}
