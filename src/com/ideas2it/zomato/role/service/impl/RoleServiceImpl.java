package com.ideas2it.zomato.role.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.dao.RoleDao;
import com.ideas2it.zomato.role.dao.impl.RoleDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for role.
 */
@Service ("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;
    
    /**
     * Adds a new role to the database.
     *
     * @param roleName
     *            - name of role to be added .
     *
     * @throws ZomatoException when adding role fails.
     */
    public void addRole(String roleName) throws ZomatoException {
        Role role = new Role(roleName);
        roleDao.insert(role);
    }
    
    /**
     * Deletes the role from database.
     *
     * @param roleId
     *            - id of role to be deleted.
     *
     * @throws ZomatoException when deleting role fails.
     */
    public void deleteRole(int roleId) throws ZomatoException {
        roleDao.delete(roleId);
    }
    
    /**
     * Returns the role specified by id.
     *
     * @param id
     *            - id of the role.
     *
     * @return role specified by id.
     *
     * @throws ZomatoException when getting role fails.
     */
    public Role getRoleById(int id) throws ZomatoException {
        return roleDao.getRoleById(id);
    }
    
    /**
     * Returns all the user role from database.
     *
     * @return set of roles.
     *
     * @throws ZomatoException when getting roles fail.
     */
    public Set getRoles() throws ZomatoException {
        return roleDao.getRoles();
    }
}
