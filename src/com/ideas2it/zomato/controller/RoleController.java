package com.ideas2it.zomato.role.controller;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.service.impl.RoleServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Controller implementation for role.
 */
@Controller ("roleController")
public class RoleController {

    @Autowired
    private RoleService roleService;
    private static final Logger logger = Logger.getLogger(RoleController.class);

    /**
     * Adds a new role.
     *
     * @param role
     *            - name of role to be added.
     *
     * @return forward to Admin Dashboard page.
     */
    @RequestMapping("/addRole")
    public ModelAndView add(@ModelAttribute("roleName") String roleName) {
        try {
            roleService.addRole(roleName);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding new role" +  roleName, e);
        }
        return new ModelAndView("forward:/toAdminDashboard");
    }
    
    /**
     * Deletes the role specified by id.
     *
     * @param roleId
     *            - id of role to be deleted.
     *
     * @return forward to Admin Dashboard page.
     */
    @RequestMapping("/removeRole")
    public ModelAndView remove(@RequestParam("roleId") int roleId) {
        try {
            roleService.deleteRole(roleId);
        } catch (ZomatoException e) {
            logger.error("Error occured while removing role with id "
                         + roleId, e);
        }
        return new ModelAndView("forward:/toAdminDashboard");
    }
    
    /**
     * Returns all the user role from database.
     *
     * @return set of roles.
     *
     * @return ModelAndView with RegisterUser page and list of user roles.
     */
    @RequestMapping("/getRoles")
    public ModelAndView getRoles() {
        Set<Role> roles = new HashSet<>();
        try {
            roles = roleService.getRoles();
        } catch (ZomatoException e) {
            logger.error("Error occured while returning all roles ", e);
        }
        return new ModelAndView("RegisterUser", "roles", roles);
    }
}
