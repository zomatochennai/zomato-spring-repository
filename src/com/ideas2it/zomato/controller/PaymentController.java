package com.ideas2it.zomato.payment.controller;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;  

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.service.PaymentService;
import com.ideas2it.zomato.payment.service.impl.PaymentServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Controller implementation for payment.
 */
@Controller ("paymentController")
@SessionAttributes ({"cartId"})
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
    private static final Logger logger = Logger
                                         .getLogger(PaymentController.class);
    
    /**
     * Redirects to the payment page.
     *
     * @return ModelAndView with PaymentPage.
     */
    @RequestMapping("/redirectToPayment")
    public String redirectToPayment() {
        return "PaymentPage";
     }
    
    /**
     * Creates a new payment for the cart id specified.
     *
     * @param customerName
     *            - customer name on the card.
     *
     * @param cardNumber
     *            - number on the card.
     *
     * @param expiryMonth
     *            - month field of the card's expiry date.
     *
     * @param expiryYear
     *            - year field of the card's expiry date.
     *
     * @return ModelAndView with OrderConfirmation page and the created payment.
     */
    @RequestMapping("/createPayment")
    public ModelAndView createPayment(
                              @ModelAttribute("cartId") int cartId, 
                              @RequestParam("customerName") String customerName,
                              @RequestParam("cardNumber") int cardNumber, 
                              @RequestParam("expiryMonth") String expiryMonth,
                              @RequestParam("expiryYear") String expiryYear) {
        Payment payment = new Payment();
        try {
            payment = paymentService.createPayment(cartId, customerName, 
                                         cardNumber, expiryMonth, expiryYear);
        } catch (ZomatoException e) {
            logger.error("Error occured while creating payment for cart "
                         + cartId, e);
        }
        return new ModelAndView("OrderConfirmation", "payment", payment);
    }
}
