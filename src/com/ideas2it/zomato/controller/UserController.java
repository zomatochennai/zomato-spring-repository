package com.ideas2it.zomato.user.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.SessionAttributes;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;

/**
 * Controller implementation for user. Forwards request parameters and models  
 * to the service layer. Redirects to the proper jsp page based on feedback from
 * service layer. Manages user session and cookies.
 */
@Controller ("userController")
@SessionAttributes ({"userId", "userFirstName", "cartId", "onlineUser",
                     "userRoleId", "restaurantId"})
public class UserController {
    
    @Autowired
    private UserService userService;
    private static final Logger logger = Logger.getLogger(UserController.class);
    
    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param user
     *            - user object about to be added.
     *
     * @param address
     *            - address of the user to be added.
     *
     * @param roleId
     *            - id of role selected by user.
     *
     * @return ModelAndView with targetPage based on successful registration.
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST) 
    public ModelAndView register(@ModelAttribute("address") Address address,
                                 @ModelAttribute("user") User user,
                                 @RequestParam("roleId") int roleId) {
        boolean success = false;
        String targetPage = "RegistrationSuccessful";
        try {
            success = userService.register(user, address, roleId);
        } catch (ZomatoException e) {
            logger.error("Error occured while registering user "
                         + user.getFirstName(), e);
        }
        return redirectIfSuccessful(success, targetPage);
    }
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param userId
     *            - id of the user.
     *
     * @param address
     *            - new address to be added.
     *
     * @return ModelAndView with targetPage based on succesful address addition.
     */
    @RequestMapping(value = "/addAddress", method = RequestMethod.POST)
    public ModelAndView addAddress(@ModelAttribute("userId") Integer userId,
                              @ModelAttribute("address") Address address) {
        boolean success = false;
        String targetPage = "CustomerHome"; 
        try {
            success = userService.addAddress(userId, address);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding new address to user "
                         + "with id " + userId, e);
        }
        return redirectIfSuccessful(success, targetPage);
    }
    
    /**
     * <p>
     * Logs in the user to the application. 
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @return ModelAndView with target page along with user id and name.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST) 
    public ModelAndView login(@RequestParam("email") String email, 
                              @RequestParam("password") String password,
                              HttpServletResponse response) {
        User user = new User();
        ModelAndView modelAndView = new ModelAndView();
        try {
            user = userService.login(email, password);
            if (null == user) {
                modelAndView = new ModelAndView("LoginUser");
                modelAndView.addObject("message", "Invalid credentials! " 
                                        + "Incorrect email and/or password! "
                                        + "Please try again!");
            } else {
                modelAndView = new ModelAndView("redirect:/homePage", 
                                   "targetPage", getUserPage(user));
                modelAndView = createUserSession(modelAndView, user, response);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while logging in user with email " 
                         + email + e);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Redirects to the user's home page.
     * </p>
     *
     * @param user
     *            - user that is logged in.
     *
     * @return home page of the user.
     */
    @RequestMapping(value = "/goToHome", method = RequestMethod.GET) 
    public String goToHome(@ModelAttribute("onlineUser") User user) {
        String userHomePage = getUserPage(user);
        return userHomePage;
    }
    
    /**
     * <p>
     * Creates the user session by adding the required fields to the 
     * user session. 
     * </p>
     *
     * @param modelAndView
     *            - modelAndView to create user session.
     *
     * @param user
     *            - user that is logged in.
     *
     * @return ModelAndView with updated user session.
     */
    private ModelAndView createUserSession(ModelAndView modelAndView, User user,
                                           HttpServletResponse response) {
        Cookie loginCookie = new Cookie("userCookieId", 
                                        Integer.toString(user.getId()));
        loginCookie.setMaxAge(60 * 60);
        response.addCookie(loginCookie);
        modelAndView.addObject("onlineUser", user);
        modelAndView.addObject("userId", user.getId());
        modelAndView.addObject("userRoleId", user.getRole().getId());
        logger.debug("role id" + user.getRole().getId());
        modelAndView.addObject("userFirstName", user.getFirstName());
        if (null != user.getActiveCart()) {
            modelAndView.addObject("cartId", user.getActiveCart().getId());
        } else {
            modelAndView.addObject("cartId", 0);
        }
        if (null != user.getRestaurant()) {
            modelAndView.addObject("restaurantId", user.getRestaurant().getId());
        } else {
            modelAndView.addObject("restaurantId", 0);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Returns the target page based on user role.
     * </p>
     *
     * @param user
     *            - user that is logged in.
     *
     * @return ModelAndView with target page based on user role.
     */
    private String getUserPage(User user) {
        String targetPage = "";
        switch (user.getRole().getId()) {
            case 1 :
                targetPage = "AdminHome";
                break;
            case 2 :
                targetPage = "CustomerHome";
                break;
            case 3 :
                targetPage = "RestaurantHome";
                break;
        }
        return targetPage;
    }
    
    /**
     * <p>
     * Redirects to RegisterUser page with empty Address object.
     * </p>         
     *
     * @return ModelAndView which redirects to getRoles along with empty 
     * Address object.
     */
    @RequestMapping(value = "/redirectToRegister")
    public ModelAndView redirectToRegister() {
        ModelAndView modelAndView = new ModelAndView("forward:/getRoles", 
                                        "address", new Address());
        modelAndView.addObject("targetLocation", "RegisterUser");
        return modelAndView;
    }
    
    /**
     * <p>
     * Redirects to the page based on the targetPage. 
     * </p>
     *
     * @param targetPage
     *            - targetPage for redirection.
     *
     * @return ModelAndView with target page.
     */
    @RequestMapping(value = "/homePage", method = RequestMethod.GET) 
    public String redirectToHome(@RequestParam("targetPage") 
                                 String targetPage) {
        return targetPage;
    }
    
    /**
     * <p>
     * Logs out the user that is already logged into the application.
     * </p>
     *
     * @param userId
     *            - id of user to be logged out.
     *
     * @return ModelAndView with targetPage based on successful logging out.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public String logout(@ModelAttribute("userId") int userId,
                               HttpServletRequest request,
                               HttpServletResponse response, Model model) {
        boolean success = false;
        String targetLocation = "forward:/clearSession";
        try {
            success = userService.logout(userId);
            if (!success) {
                targetLocation = "SomethingWentWrong";
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while logging out user with id "
                         + userId, e);
        }
        return targetLocation;
    }
    
    /**
     * <p>
     * Redirects to the target page based on the boolean status.
     * </p>       
     *
     * @param success
     *            - boolean that decides the redirect page.
     *
     * @param targetPage
     *            - the redirect page if success is true.
     *
     * @return ModelAndView with targetPage based on boolean status.
     */
    private ModelAndView redirectIfSuccessful(boolean success, 
                                              String targetPage) {
        ModelAndView modelAndView = new ModelAndView("SomethingWentWrong");
        if (success) {
            modelAndView = new ModelAndView("redirect:/redirectToPage");
            modelAndView.addObject("targetPage", targetPage);
            
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Enables user to reset old password after checking the security questions
     * petName and bicycleName.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user.
     *
     * @return ModelAndView with targetPage based on successful password reset.
     */
    @RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
    public ModelAndView resetPassword(@RequestParam("email") String email,
                              @RequestParam("password") String password,
                              @RequestParam("petName") String petName, 
                              @RequestParam("bicycleName") String bicycleName) {
        boolean success = false;
        ModelAndView modelAndView = new ModelAndView();
        try {
            success = userService.resetPassword(email, password, petName,
                                              bicycleName);
            if (success) {
                modelAndView = new ModelAndView("LoginUser");
                modelAndView.addObject("message", "Password reset successful, " 
                                        + "Please login here!!");
            } else {
                modelAndView = new ModelAndView("ForgotPassword");
                modelAndView.addObject("message", "Email and/or answers to "
                                       + "security questions are incorrect! " 
                                       + "Please try again!");
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while resetting password for user"
                         + " with email " + email, e);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Enables user to change his existing password.
     * </p>
     *
     * @param oldPassword
     *            - oldPassword of the user.
     *
     * @param newPassword
     *            - newPassword entered by the user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return .
     */
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ModelAndView changePassword(@RequestParam("oldPassword") 
                                      String oldPassword,
                                      @RequestParam("newPassword") 
                                      String newPassword,
                                      @ModelAttribute("userId") int userId) {
        boolean success = false;
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        try {
            success = userService.changePassword(oldPassword, newPassword,
                                                 userId);
            user = userService.getUserById(userId);
            if (success) {
                modelAndView = new ModelAndView(getUserPage(user));
                //modelAndView = createUserSession(modelAndView, user);
                modelAndView.addObject("message", "Password changed "
                                       + "successfully!");
            } else {
                modelAndView = new ModelAndView("ChangePassword");
                modelAndView.addObject("message", "Password incorrect! " 
                                       + "Please try again!");
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while changing password for user"
                         + userId, e);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param user
     *            - user object to be updated.
     *
     * @throws ZomatoException when user update fails.
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST) 
    public ModelAndView updateUser(@ModelAttribute("userId") int userId,
                                   @RequestParam("firstName") String firstName,
                                   @RequestParam("lastName") String lastName,
                                   @RequestParam("mobile") long mobile,
                                   @RequestParam("email") String email) {
        String targetPage = " ";
        ModelAndView modelAndView = new ModelAndView();
        try {
            User user = userService.updateUser(userId, firstName, lastName,
                                               mobile, email);
            targetPage = getUserPage(user);
            if (null != user) {
                modelAndView = new ModelAndView(getUserPage(user), "message", 
                               "Details updated successfully!!");
                //modelAndView = createUserSession(modelAndView, user);
            } else {
                modelAndView = new ModelAndView("SomethingWentWrong");
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while updating user " + userId, e);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Redirects to the page specified.
     * </p>         
     *
     * @return ModelAndView with ths target page.
     */
    @RequestMapping(value = "/redirectToPage")
    public ModelAndView redirectToPage(@RequestParam("targetPage") 
                                       String targetPage) {
        return new ModelAndView(targetPage);
    }
}
