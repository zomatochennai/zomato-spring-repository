package com.ideas2it.zomato.restaurant.controller;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;  

/**
 * Controller implementation for restaurant. Restaurant and classification
 * related operations are done through this controller.
 */
@Controller ("adminController")
public class AdminController {

    /**
     * Populates the admin dashboard with classification, category, role and
     * food list.
     *
     * @return forwards to getAllClassifications .
     */
    @RequestMapping("/toAdminDashboard")
    public String populateAdminDashboard() {
        return "forward:/getAllClassifications";
    }
}
