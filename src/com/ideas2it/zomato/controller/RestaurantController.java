package com.ideas2it.zomato.restaurant.controller;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;  

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.service.ClassificationService;
import com.ideas2it.zomato.classification.service.impl.ClassificationServiceImpl;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.restaurant.service.RestaurantService;
import com.ideas2it.zomato.restaurant.service.impl.RestaurantServiceImpl;

/**
 * Controller implementation for restaurant. Restaurant and classification
 * related operations are done through this controller.
 */
@Controller ("restaurantController")
@SessionAttributes ({"userId", "restaurantId"})
public class RestaurantController {
    
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private ClassificationService classificationService;
    private static final Logger logger = Logger
                                         .getLogger(RestaurantController.class); 

    /**
     * Registers a new restaurant to the database.
     *
     * @param restaurant
     *            - restaurant object to be registered .
     *
     * @param address
     *            - address of the restaurant.
     *
     * @param userId
     *            - id of restaurant's vendor.
     *
     * @return ModelAndView with RestaurantHome page.
     */
    @RequestMapping("/registerRestaurant")
    public ModelAndView registerRestaurant(
                           @ModelAttribute("restaurant") Restaurant restaurant,
                           @ModelAttribute("address") Address address,
                           @ModelAttribute("userId") int userId) {
        boolean success = false;
        ModelAndView modelAndView = new ModelAndView();
        try {
            logger.debug("restaurant name " + restaurant.getName());
            success = restaurantService.registerRestaurant(restaurant, address,
                                                         userId);
            if (success) {
                modelAndView = new ModelAndView("RestaurantHome", "message", 
                                "Restaurant registered successfully!!!");
            } else {
                modelAndView = new ModelAndView("SomethingWentWrong");
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while registering restaurant "
                         + restaurant.getName(), e);
        }
        return modelAndView;
    }
    
    /**
     * Returns all the restaurants in the database for the specified 
     * classification id.
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched.
     *
     * @return ModelAndView with RestaurantList page and list of restaurants
     *         for the selected classification.
     */
    @RequestMapping(value = "/getRestaurants", method = RequestMethod.GET)
    public ModelAndView getRestaurants(
                       @RequestParam("id") String classificationId) {
        Set<Restaurant> restaurants = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            restaurants = restaurantService.getRestaurantsByClassificationId(
                                            Integer.parseInt(classificationId));
            if (restaurants.isEmpty()) {
                modelAndView = new ModelAndView("forward:/getClassifications", 
                               "message", "Sorry!! the selected classification "
                               + "does not have any restaurants!!");
            } else {
                modelAndView = new ModelAndView("RestaurantList", 
                                                "restaurants", restaurants);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting restaurants "
                         + "with classification id " + classificationId, e);
        }
        return modelAndView;
    }
    
    /**
     * Adds a new classification.
     *
     * @param classificationName
     *            - name of classification to be added.
     *
     * @return forward to specified targetLocation.
     */
    @RequestMapping("/addClassification")
    public ModelAndView addClassification(@RequestParam("targetLocation") 
                                          String targetLocation,
                                          @ModelAttribute("classificationName") 
                                          String classificationName) {
        logger.debug("adding classification " + classificationName);
        try {
            classificationService.addClassification(classificationName);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding classification "
                         + classificationName, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Removes the classification based on classificationId.
     *
     * @param classificationId
     *            - id of classification to be removed.
     *
     * @return forward to specified targetLocation.
     */
    @RequestMapping("/removeClassification")
    public ModelAndView removeClassification(@RequestParam("targetLocation") 
                                             String targetLocation,
                                             @RequestParam("classificationId") 
                                             int classificationId) {
        try {
            classificationService.removeClassification(classificationId);
        } catch (ZomatoException e) {
            logger.error("Error occured while removing classification with id "
                         + classificationId, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Returns all the classifications in the database.
     *
     * @return ModelAndView with ClassificationList page along with 
     *         list of all classifications, or returns to CustomerHome if
     *         classifications are empty.
     */
    @RequestMapping("/getClassifications")
    public ModelAndView getClassifications() {
        Set<Classification> classifications = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            classifications = classificationService.getClassifications();
            if (null == classifications) {
                modelAndView = new ModelAndView("CustomerHome", "message", 
                               "Oops!! Unable to order food now, Please try "
                               + "again later!!");
            } else {
                modelAndView = new ModelAndView("ClassificationList", 
                                "classifications", classifications);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting all classifications", e);
        }
        return modelAndView;
    }
    
    /**
     * Returns all the restaurants in the database by restaurantName.
     *
     * @param restaurantName
     *            - name of restaurants to be fetched.
     *
     * @return ModelAndView with RestaurantList page and list of restaurants
     *         for the specified restaurantName.
     */
    @RequestMapping(value = "/getRestaurantsByName", method = RequestMethod.GET)
    public ModelAndView getRestaurantsByName(
                        @RequestParam("name") String restaurantName) {
        Set<Restaurant> restaurants = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            restaurants = restaurantService.getRestaurantsByName(restaurantName);
            if (restaurants.isEmpty()) {
                modelAndView = new ModelAndView("SamePage", "message",
                               "Sorry! We could not find any restaurant for "
                             + "the search term " + restaurantName 
                             + "Please try again!!");
            } else {
                modelAndView = new ModelAndView("NewPage",
                                                "restaurants", restaurants);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting restaurants "
                       + "with search term " + restaurantName, e);
        }
        return modelAndView;
    }
    
    /**
     * Updates the restaurant and its address to the database.
     *
     * @param restaurant
     *            - restaurant object to be updated.
     *
     * @param address
     *            - address of restaurant to be updated .
     *
     * @return ModelAndView with ManageRestaurant page with updated restaurant.
     */
    @RequestMapping("/updateRestaurant")
    public ModelAndView updateRestaurant(@ModelAttribute("restaurant") 
                                         Restaurant restaurant,
                                         @ModelAttribute("address") 
                                         Address address) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            Restaurant updatedRestaurant = restaurantService
                                    .updateRestaurant(restaurant, address);
            modelAndView = new ModelAndView("ManageRestaurant", "restaurant",
                                             updatedRestaurant);
            modelAndView.addObject("message", "Restaurant updated "
                                   + "successfully!!!");
        } catch (ZomatoException e) {
            logger.error("Error occured while updating restaurant "
                         + restaurant.getId(), e);
        }
        return modelAndView;
    }
    
    /**
     * <p>
     * Gets the restaurant for the specified restaurantId.
     * </p>         
     *
     * @param restaurantId
     *            - id of restaurant to be fetched.
     *
     * @return ModelAndView with  ManageRestaurant page or to NewRestaurantPage
     *         if restaurant does not exist.
     */
    @RequestMapping(value = "/manageRestaurant")
    public ModelAndView getRestaurant(@ModelAttribute("restaurantId")
                                                    int restaurantId) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            if (null == (Integer)restaurantId || 0 == restaurantId) {
                modelAndView = new ModelAndView("NewRestaurantPage", 
                               "restaurant", new Restaurant());
            } else {
                modelAndView = new ModelAndView("ManageRestaurant", "restaurant", 
                             restaurantService.getRestaurantById(restaurantId));
                modelAndView.addObject("classifications", 
                                  classificationService.getClassifications());
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while updating restaurant with id" 
                         + restaurantId, e);
        }
        return modelAndView;
    }
    
    /**
     * Deletes a menuItem from the restaurant specified by id.
     *
     * @param restaurantId
     *            - id of restaurant whose menuItem needs to be deleted.
     *
     * @param menuItemId
     *            - id of menuItem to be deleted.
     *
     * @return forwards to manageMenu.
     */
    @RequestMapping("/deleteMenuItem")
    public ModelAndView deletemenuItem(@ModelAttribute("restaurantId") 
                                       int restaurantId,
                                       @RequestParam("menuItemId") 
                                       int menuItemId) {
        try {
            restaurantService.deleteMenuItem(restaurantId, menuItemId);
        } catch (ZomatoException e) {
            logger.error("Error occured while deleting menuItem " 
                         + menuItemId, e);
        }
        return new ModelAndView("forward:/manageMenu");
    }

    /**
     * Returns all the classifications in the database.
     *
     * @return forward set of classifications to categoriesAndFood.
     */
    @RequestMapping("/ClassificationsForAdmin")
    public ModelAndView getAllClassifications() {
        Set<Classification> classifications = new HashSet<>();
        try {
            classifications = classificationService.getClassifications();
        } catch (ZomatoException e) {
            logger.error("Error occured while getting classifications", e);
        }
        return new ModelAndView("forward:/categoriesAndFood",
                                        "classifications", classifications);
    }
    
    /**
     * <p>
     * Fetches the restaurant for the id specified.
     * </p>
     *
     * @param restaurantId
     *            - id of restaurant to be fetched.         
     *
     * @return forward to NewRestaurantPage or manageCategoriesAndFood based
     *         restaurant existance.
     */
    @RequestMapping(value = "/manageMenu")
    public ModelAndView manageMenuItems(@ModelAttribute("restaurantId")
                                                    int restaurantId) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            if (null == (Integer)restaurantId || 0 == restaurantId) {
                modelAndView = new ModelAndView("NewRestaurantPage", 
                               "restaurant", new Restaurant());
                modelAndView.addObject("message", "You do not have a " 
                                       + "restaurant yet!! Create one here!!");
            } else {
                Restaurant restaurant = restaurantService
                                .getRestaurantById(restaurantId);
                modelAndView = new ModelAndView("forward:/manageCategoriesAndFood",  
                                                "restaurant", restaurant);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while updating restaurant "
                         + restaurantId, e);
        }
        return modelAndView;
    }
    
    /**
     * Assigns the classifications to the restaurant specified by id.
     *
     * @param restaurantId
     *            - id of restaurant to be fetched.
     *
     * @param classificationIdList
     *            - ids of classifications to be assigned to the restaurant.
     *
     * @return forward to manageRestaurant.
     */
    @RequestMapping("/assignClassifications")
    public ModelAndView assignClassifications(@ModelAttribute("restaurantId")
                                      int restaurantId,
                                      @RequestParam("classificationIdList")
                                      String[] classificationIds) {
        try {
            restaurantService.assignClassifications(restaurantId, classificationIds);
        } catch (ZomatoException e) {
            logger.error("Error occured while assigning classifications to "
                         + "restaurant " + restaurantId, e);
        }
         return new ModelAndView("forward:/manageRestaurant");
    }
    
    /**
     * Removes the classification from the restaurant.
     *
     * @param restaurantId
     *            - id of restaurant whose classification needs to be removed.
     *
     * @param classificationId
     *            - id of classifications to be removed from the restaurant.
     *
     * @return forward to manageRestaurant.
     */
    @RequestMapping("/removeRestaurantClassification")
    public ModelAndView removeRestaurantClassification(@ModelAttribute
                                     ("restaurantId") int restaurantId,
                                     @RequestParam("classificationId") 
                                     int classificationId) {
        try {
            restaurantService.removeRestaurantClassification(restaurantId,
                                                              classificationId);
        } catch (ZomatoException e) {
            logger.error("Error occured while removing classification with id "
                         + classificationId, e);
        }
        return new ModelAndView("forward:/manageRestaurant");
    }
    
    /**
     * Populates the admin dashboard with classification, category, role and
     * food list.
     *
     * @return forwards to ClassificationsForAdmin .
     */
    @RequestMapping("/toAdminDashboard")
    public String populateAdminDashboard() {
        return "forward:/ClassificationsForAdmin";
    }
}
