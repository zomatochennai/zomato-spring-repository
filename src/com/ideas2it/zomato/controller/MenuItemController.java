package com.ideas2it.zomato.menuItem.controller;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;  

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.service.impl.MenuItemServiceImpl;
import com.ideas2it.zomato.food.service.FoodService;
import com.ideas2it.zomato.food.service.impl.FoodServiceImpl;
import com.ideas2it.zomato.category.service.CategoryService;
import com.ideas2it.zomato.category.service.impl.CategoryServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Controller implementation for menuItem. All menuItem and categories related 
 * operations are done through this controller.
 */
@Controller("menuItemController")
@SessionAttributes ("restaurantId")
public class MenuItemController {

    @Autowired
    private MenuItemService menuItemService;
    @Autowired
    private FoodService foodService;
    @Autowired
    private CategoryService categoryService;
    private static final Logger logger = Logger
                                         .getLogger(MenuItemController.class); 
       
    /**
     * Updates a existing menuItem.
     *
     * @param menuItem
     *            - menuItem object about to be updated.
     *
     * @return ModelAndView with RestaurantHome page.
     */
    @RequestMapping("/updateMenuItem")
    public ModelAndView updateMenuItem(@ModelAttribute("menuItem") MenuItem menuItem) {
        try {
            menuItemService.updateMenuItem(menuItem);
        } catch (ZomatoException e) {
            logger.error("Error occured while updating menuItem "
                         + menuItem.getName(), e);
        }
        return new ModelAndView("RestaurantHome");
    }
    
    /**
     * Get the menu item categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return ModelAndView with Restaurant page, restaurant id and categories 
     *         list for specified restaurant.
     */
    @RequestMapping("/getCategories")
    public ModelAndView getCategories(@RequestParam ("id") int restaurantId) {
        Set<Category> categories = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            categories = categoryService.getCategories(restaurantId);
            if (categories.isEmpty()) {
                modelAndView = new ModelAndView("RestaurantList", "message",
                                   "Sorry!! This restaurant does not have any "
                                   + "food categories!!");
            } else {
                modelAndView = new ModelAndView("Restaurant", "categories",
                                                      categories);
                modelAndView.addObject("restaurantId", restaurantId);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting categories for "
                         + "restaurant with id " + restaurantId, e);
        }
        return modelAndView;
    }
    
    /**
     * Get all the categories and foods from the database.
     *
     * @return ModelAndView with AdminDashboard page and all categories, foods.
     */
    @RequestMapping("/categoriesAndFood")
    public ModelAndView getAllCategoriesAndFood() {
        Set<Category> categories = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            categories = categoryService.getAllCategories();
            modelAndView = new ModelAndView("AdminDashboard", 
                                        "categories", categories);
            modelAndView.addObject("foods", foodService.getFoods());
        } catch (ZomatoException e) {
            logger.error("Error occured while getting all categories ", e);
        } 
        return modelAndView;
    }
    
    /**
     * Get the menu items for the specified restaurant and category.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return ModelAndView with FoodItemList page and menuItem list for the
     *         specified restaurant category.
     */
    @RequestMapping("/getMenuItems")
    public ModelAndView getMenuItems(
                              @ModelAttribute("restaurantId") int restaurantId,
                              @RequestParam("id") int categoryId) {
        Set<MenuItem> menuItems = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            menuItems = menuItemService.getMenuItems(restaurantId, categoryId);
            if (menuItems.isEmpty()) {
                modelAndView = new ModelAndView("Restaurant", "message",
                                   "Sorry!! the selected category does not "
                                   + "have any menu items!!");
            } else {
                modelAndView = new ModelAndView("FoodItemList", 
                                                     "menuItems", menuItems);
                modelAndView.addObject("categoryId", categoryId);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting menuItems for restaurant "
                         + "with id " + restaurantId, e);
        }
        return modelAndView;
    }
    
    /**
     * Adds a new food to the database.
     *
     * @param targetLocation
     *            - targetLocation for modelAndView.
     *
     * @param foodName
     *            - name food to be added.
     *
     * @return ModelAndView with targetLocation.
     */
    @RequestMapping("/addFood")
    public ModelAndView addFood(@RequestParam("targetLocation") 
                                String targetLocation,
                                @RequestParam("foodName") String foodName) {
        logger.debug("adding food " + foodName);
        try {
            foodService.addFood(foodName);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding food " + foodName, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Removes the food from the database.
     *
     * @param targetLocation
     *            - targetLocation for modelAndView.
     *
     * @param foodId
     *            - id of food to be removed.
     *
     * @return ModelAndView with targetLocation.
     */
    @RequestMapping("/removeFood")
    public ModelAndView removeFood(@RequestParam("targetLocation") 
                                   String targetLocation,
                                   @RequestParam("foodId") int foodId) {
        try {
            foodService.removeFood(foodId);
        } catch (ZomatoException e) {
            logger.error("Error occured while removing food " + foodId, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Adds a new category to the database.
     *
     * @param targetLocation
     *            - targetLocation for modelAndView.
     *
     * @param CategoryName
     *            - name of category to be added.
     *
     * @return forward to Admin Dashboard page.
     */
    @RequestMapping("/addCategory")
    public ModelAndView addCategory(@RequestParam("targetLocation") 
                                    String targetLocation,
                                    @RequestParam("categoryName")
                                    String categoryName) {
        try {
            categoryService.addCategory(categoryName);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding category "
                         + categoryName, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Removes the category from the database specified by id.
     *
     * @param targetLocation
     *            - targetLocation for modelAndView.
     *
     * @param categoryId
     *            - id of category to be removed.
     *
     * @return forward to Admin Dashboard page.
     */
    @RequestMapping("/removeCategory")
    public ModelAndView removeCategory(@RequestParam("targetLocation") 
                                       String targetLocation,
                                       @RequestParam("categoryId") 
                                       int categoryId) {
        try {
            categoryService.removeCategory(categoryId);
        } catch (ZomatoException e) {
            logger.error("Error occured while removing category " 
                         + categoryId, e);
        }
        return new ModelAndView(targetLocation);
    }
    
    /**
     * Get all the categories and foods from the database.
     *
     * @return ModelAndView with ManageMenu page and all categories and foods.
     */
    @RequestMapping("/manageCategoriesAndFood")
    public ModelAndView manageCategoriesAndFood() {
        Set<Category> categories = new HashSet<>();
        ModelAndView modelAndView = new ModelAndView();
        try {
            categories = categoryService.getAllCategories();
            modelAndView = new ModelAndView("ManageMenu", 
                                            "categories", categories);
            modelAndView.addObject("foods", foodService.getFoods());
            modelAndView.addObject("menuItem", new MenuItem());
        } catch (ZomatoException e) {
            logger.error("Error occured while getting all categories ", e);
        } 
        return modelAndView;
    }
    
    /**
     * Registers a new menuItem to the restaurant specified by id.
     *
     * @param restaurantId
     *            - id of restaurant to which menu items need to be added.
     *
     * @param menuItem
     *        - menuItem to be added
     *
     * @param foodIds
     *            - ids of food that belongs to the menuItem.
     *
     * @param categoryIds
     *        - ids of category that belongs to the menuItem.
     *
     * @return forwards request to manageMenu.
     */
    @RequestMapping("/addMenuItem")
    public ModelAndView addMenuItem(@ModelAttribute("restaurantId") int restaurantId,
                            @ModelAttribute("menuItem") MenuItem menuItem,
                            @RequestParam("foodList") String[] foodIds,
                            @RequestParam("categoryList")
                            String[] categoryIds) {
        try {
            menuItemService.addMenuItem(restaurantId, menuItem, foodIds, 
                                        categoryIds);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding menuItems to restaurant "
                         + restaurantId, e);
        } 
        return new ModelAndView("forward:/manageMenu");
    }
}
