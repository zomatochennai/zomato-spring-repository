package com.ideas2it.zomato.user.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.SessionAttributes;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;

/**
 * Controller implementation for user. Forwards request parameters and models  
 * to the service layer. Redirects to the proper jsp page based on feedback from
 * service layer. Manages user session and cookies.
 */
@Controller ("logoutController")
public class LogoutController {

    @Autowired
    private UserService userService;
    private static final Logger logger = Logger.getLogger(UserController.class);

    /**
     * <p>
     * Logs out the user that is already logged into the application.
     * </p>
     *
     * @param userId
     *            - id of user to be logged out.
     *
     * @return ModelAndView with targetPage based on successful logging out.
     */
    @RequestMapping(value = "/clearSession")
    public String logout(HttpServletRequest request,
                               HttpServletResponse response, Model model) {
        clearCookies(request, response);
        HttpSession session = request.getSession(false);
        session.invalidate();
        return "Welcome";
    }
    
    /**
     * <p>
     * Clears the cookies by setting its max age to zero.. 
     * </p>
     */
    public void clearCookies(HttpServletRequest request,
                             HttpServletResponse response) {
        Cookie loginCookie = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for(Cookie cookie : cookies) {
                if (cookie.getName().equals("userCookieId")) {
                    loginCookie = cookie;
                    break;
                }
            }
        }
        if(loginCookie != null) {
            loginCookie.setMaxAge(0);
            response.addCookie(loginCookie);
        }
    }
}
