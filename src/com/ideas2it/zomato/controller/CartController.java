package com.ideas2it.zomato.cart.controller;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.SessionAttributes;  
import org.springframework.web.bind.annotation.RequestParam;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.cart.service.impl.CartServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Controller implementation for user cart.
 */
@Controller ("cartController")
@SessionAttributes ({"userId", "cartId"})
public class CartController {

    @Autowired
    private CartService cartService;
    private static final Logger logger = Logger.getLogger(CartController.class); 
    
    /**
     * Adds menuItem selected by id to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item.
     *
     * @param quantity
     *            - quantity of menuItem.
     *
     * @param userId
     *            - id of cart's user.
     *
     * @return forwards the request to getMenuItems along with category id and
     *         cart id.
     */
    @RequestMapping("/addToCart")
    public String addMenuItemToCart(@RequestParam("id") int menuItemId, 
                                    @RequestParam("quantity") int quantity,
                                    @RequestParam("categoryId") int categoryId,
                                    @ModelAttribute("userId") Integer userId,
                                    Model model) {
        int cartId = 0;
        try {
            cartId = cartService.addMenuItemToCart(menuItemId, quantity, userId);
        } catch (ZomatoException e) {
            logger.error("Error occured while adding menuItem to cart "
                         + menuItemId, e);
        }
        model.addAttribute("cartId", cartId);
        model.addAttribute("message", "Added to cart successfully!!!");
        return "forward:/getMenuItems?categoryId=categoryId&cartId=cartId";
    }
    
    /**
     * Returns the cart for the specified cart id.
     *
     * @param cartId
     *            - id of cart.
     *
     * @return ModelAndView with BlankShoppingCart page if cart id is zero or 
     *         returns ShoppingCart page along with cart object.
     */
    @RequestMapping("/viewCart")
    public ModelAndView getCart(@ModelAttribute("cartId") int cartId) {
        Cart cart = new Cart();
        ModelAndView modelAndView = new ModelAndView();
        try {
            if (cartId == 0) {
                modelAndView = new ModelAndView("BlankShoppingCart");
            } else {
                cart = cartService.getCartById(cartId);
                modelAndView = new ModelAndView("ShoppingCart", "cart", cart);
            }
        } catch (ZomatoException e) {
            logger.error("Error occured while getting cart " + cartId, e);
        }
        return modelAndView;
    }
}
