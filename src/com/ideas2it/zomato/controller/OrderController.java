package com.ideas2it.zomato.order.controller;

import java.lang.NullPointerException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;  
import org.springframework.web.bind.annotation.PathVariable;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;  
import org.springframework.web.servlet.ModelAndView;  
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;  

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.service.OrderService;
import com.ideas2it.zomato.order.service.impl.OrderServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Controller implementation for order.
 */
@Controller ("orderController")
@SessionAttributes ({"cartId"})
public class OrderController {

    @Autowired
    private OrderService orderService;
    private static final Logger logger = Logger.getLogger(OrderController.class);
    
    /**
     * Returns the order and details for the specified cart id.
     *
     * @param cartId
     *            - cart id for the required order .
     *
     * @return ModelAndView with OrderPage page and order created for the cart.
     */
    @RequestMapping("/viewOrder")
    public ModelAndView getOrder(@ModelAttribute("cartId") int cartId) {
        Order order = new Order();
        try {
            order = orderService.getOrderByCartId(cartId);
        } catch (ZomatoException e) {
            logger.error("Error occured while getting order for cart "
                         + cartId, e);
        }
        return new ModelAndView("OrderPage", "order", order);
    }
    
    /**
     * Creates a new order for the cart.
     *
     * @param cartId
     *            - cart id for the order to be created.
     *
     * @return ModelAndView with OrderPage page and order created for the cart.
     */
    @RequestMapping("/createOrder")
    public ModelAndView createOrder(@ModelAttribute("cartId") int cartId) {
        try {
            orderService.createOrder(cartId);
        } catch (ZomatoException e) {
            logger.error("Error occured while creating order for user "
                         + cartId, e);
        }
        return getOrder(cartId);
    }
    
    /**
     * Confirms the order for the cart.
     *
     * @param paymentId
     *            - payment id for the order .
     *
     * @return ModelAndView with OrderConfirmation page.
     */
    @RequestMapping("/confirmOrder")
    public ModelAndView confirmOrder(@RequestParam("paymentId") int paymentId) {
        Order order = new Order();
        try {
            order = orderService.confirmOrder(paymentId);
        } catch (ZomatoException e) {
            logger.error("Error occured while confirming order for payment "
                         + paymentId, e);
        }
        return new ModelAndView("OrderConfirmation");
    }
}
