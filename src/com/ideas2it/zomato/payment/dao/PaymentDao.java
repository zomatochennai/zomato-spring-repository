package com.ideas2it.zomato.payment.dao;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This interface handles access between Payment service and hibernate .
 */
public interface PaymentDao {
   
   /**
     * Updates the cart into the database.
     *
     * @param payment
     *            - payment to be updated.
     *
     * @throws ZomatoException when inserting payment fails.
     */
    void insert(Payment payment) throws ZomatoException;
   
    /**
     * Returns the payment for the specified id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment object for the id.
     *
     * @throws ZomatoException when getting payment by id fails.
     */
    Payment getPaymentById(int id) throws ZomatoException;
    
    /**
     * Updates the payment in the database.
     *
     * @param payment
     *            - payment object about to be updated.
     *
     * @throws ZomatoException when updating payment fails.
     */
    void update(Payment payment) throws ZomatoException;
}
