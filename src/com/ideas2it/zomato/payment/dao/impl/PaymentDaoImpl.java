package com.ideas2it.zomato.payment.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.dao.PaymentDao;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This class handles access between Payment services and hibernate .
 */
@Repository("paymentDao")
public class PaymentDaoImpl implements PaymentDao {
    
    private SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(PaymentDaoImpl.class); 
    
	/**
     * Updates the cart into the database.
     *
     * @param payment
     *            - payment to be updated.
     *
     * @throws ZomatoException when inserting payment fails.
     */
    public void insert(Payment payment) throws ZomatoException {
        sessionFactory.getCurrentSession().save(payment); 
    }
    
    /**
     * Returns the payment for the specified id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment object for the id.
     *
     * @throws ZomatoException when getting payment by id fails.
     */
    public Payment getPaymentById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Payment.class);
        criteria.add(Restrictions.eq("id", id));
        return (Payment) criteria.uniqueResult();
    }
    
    /**
     * Updates the payment in the database.
     *
     * @param payment
     *            - payment object about to be updated.
     *
     * @throws ZomatoException when updating payment fails.
     */
    public void update(Payment payment) throws ZomatoException {
        sessionFactory.getCurrentSession().update(payment);
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
