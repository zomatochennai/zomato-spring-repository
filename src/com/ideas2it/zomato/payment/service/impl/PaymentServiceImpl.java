package com.ideas2it.zomato.payment.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.service.OrderService;
import com.ideas2it.zomato.order.service.impl.OrderServiceImpl;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.service.PaymentService;
import com.ideas2it.zomato.payment.dao.PaymentDao;
import com.ideas2it.zomato.payment.dao.impl.PaymentDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This class handles payment related functionalities.
 */
@Service ("paymentService")
@Transactional
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentDao paymentDao;
    @Autowired
    private OrderService orderService;
    
    /**
     * Returns the payment for the specified id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment for the specified id.
     *
     * @throws ZomatoException when getting payment fails.
     */
    public Payment getPaymentById(int id) throws ZomatoException {
        return paymentDao.getPaymentById(id);
    }
    
    /**
     * Creates a new payment for the cart.
     *
     * @param cart
     *            - id of the cart.
     *
     * @param customerName
     *            - customer name on the card.
     *
     * @param cardNumber
     *            - number on the card.
     *
     * @param expiryMonth
     *            - month field of the card's expiry date.
     *
     * @param expiryYear
     *            - year field of the card's expiry date.
     *
     * @return newly created payment for the cart.
     *
     * @throws ZomatoException when creating payment fails.
     */
    public Payment createPayment(int cartId, String customerName,
                              int cardNumber, String expiryMonth, 
                              String expiryYear) throws ZomatoException {
        Order order = orderService.getOrderByCartId(cartId);
        order.getCart().setActive(false);
        Payment payment = new Payment(order);
        payment.setCustomerName(customerName);
        payment.setCardNumber(cardNumber);
        payment.setExpiryDate(expiryMonth + "/" + expiryYear);
        order.setPayment(payment);
        payment.getOrder().setPaymentDone(true);
        paymentDao.insert(payment);
        return payment;
    }
}
