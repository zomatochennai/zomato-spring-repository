package com.ideas2it.zomato.payment.service;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.payment.dao.PaymentDao;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * Interface implementation for payment service class. 
 */
public interface PaymentService {
    
    /**
     * Returns the payment for the id.
     *
     * @param id
     *            - id of the payment.
     *
     * @return payment specified by id.
     *
     * @throws ZomatoException when getting payment fails.
     */
    Payment getPaymentById(int id) throws ZomatoException;
    
    /**
     * Creates a new payment for the cart.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @param customerName
     *            - customer name on the card.
     *
     * @param cardNumber
     *            - number on the card.
     *
     * @param expiryMonth
     *            - Expiry month of the card.
     * 
     * @param expiryYear
     *            - Expiry year of the card.
     *
     * @return newly created payment for the cart.
     *
     * @throws ZomatoException when creating payment fails.
     */
    Payment createPayment(int cartId, String customerName,
                              int cardNumber, String expiryMonth, 
                              String expiryYear) throws ZomatoException;
}
