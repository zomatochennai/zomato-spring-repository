package com.ideas2it.zomato.exception;

/**
 * Implementation of custom exception class.
 */
public class ZomatoException extends Exception {

    public ZomatoException(String message) {
        super(message);
    }
}
