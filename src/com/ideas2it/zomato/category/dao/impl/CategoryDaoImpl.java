package com.ideas2it.zomato.category.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.category.dao.CategoryDao;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Performs all the database related funcionalities for category object.
 */
@Repository("categoryDao")
public class CategoryDaoImpl implements CategoryDao {

    private SessionFactory sessionFactory;

    /**
     * Inserts a new address into the database.
     *
     * @param category
     *            - category object about to be inserted.
     *
     * @throws ZomatoException when inserting category fails.
     */
    public void insert(Category category) throws ZomatoException {
        sessionFactory.getCurrentSession().save(category); 
    }
    
    /**
     * Gets the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     *
     * @throws ZomatoException when getting categories fail.
     */
    public Set getCategories(int restaurantId) 
                         throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Category.class)
                                   .createCriteria("menuItems")
                                   .createCriteria("restaurant");
        criteria.add(Restrictions.eq("id", restaurantId));
        return new HashSet(criteria.list());
    }
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     *
     * @throws ZomatoException when getting all categories fail.
     */
    public Set getAllCategories() throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createQuery("FROM Category").list());
    }
    
    /**
     * Deletes the category from the database specified by id.
     *
     * @param id
     *            - id of category to be deleted.
     *
     * @throws ZomatoException when deleting restaurant fails.
     */
    public void delete(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Category category = (Category)session.get(Category.class, id);
        session.delete(category);
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Gets the Category from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return food specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    public Category getCategoryById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Category) session.get(Category.class, id);
    }
}
