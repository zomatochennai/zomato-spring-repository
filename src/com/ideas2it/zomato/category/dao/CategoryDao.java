package com.ideas2it.zomato.category.dao;

import java.util.Set;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Category dao class.
 */
public interface CategoryDao {

    /**
     * Inserts a new address into the database.
     *
     * @param category
     *            - category object about to be inserted.
     *
     * @throws ZomatoException when inserting category fails.
     */
    void insert(Category category) throws ZomatoException;
    
    /**
     * Gets the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     *
     * @throws ZomatoException when getting categories fail.
     */
    Set getCategories(int restaurantId) throws ZomatoException;
    
    /**
     * Get all the categories from the database.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required .
     *
     * @return all food catgories belonging to the restaurant.
     *
     * @throws ZomatoException when getting all categories fail.
     */
    Set getAllCategories() throws ZomatoException;
    
    /**
     * Deletes the category from the database specified by id.
     *
     * @param id
     *            - id of category to be deleted.
     *
     * @throws ZomatoException when deleting restaurant fails.
     */
    void delete(int id) throws ZomatoException;
    
    /**
     * Gets the Category from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return food specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    Category getCategoryById(int id) throws ZomatoException;
}
