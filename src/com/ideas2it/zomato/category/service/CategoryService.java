package com.ideas2it.zomato.category.service;

import java.util.Set;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for category service class.
 */
public interface CategoryService {

    /**
     * Adds a new category to database.
     *
     * @param categoryName
     *            - name of category to be added 
     *
     * @throws ZomatoException when adding category fails.
     */
    void addCategory(String categoryName) throws ZomatoException;
    
    /**
     * Returns all the categories for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all catgories belonging to the restaurant
     *
     * @throws ZomatoException when getting categories for restaurant fails.
     */
    Set getCategories(int restaurantId) throws ZomatoException;
    
    /**
     * Get all the categories from the database.
     *
     * @return all food catgories from the database.
     *
     * @throws ZomatoException when getting all categories fail.
     */
    Set getAllCategories() throws ZomatoException;
    
    /**
     * Removes the category specified by id from the database.
     *
     * @param id
     *            - id of category
     *
     * @throws ZomatoException when removing category fails.
     */
    void removeCategory(int id) throws ZomatoException;
    
    /**
     * Gets the Category from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return food specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    Category getCategoryById(int id) throws ZomatoException;
}
