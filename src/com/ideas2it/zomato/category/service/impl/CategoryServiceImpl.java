package com.ideas2it.zomato.category.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.category.service.CategoryService;
import com.ideas2it.zomato.category.dao.CategoryDao;
import com.ideas2it.zomato.category.dao.impl.CategoryDaoImpl;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for category .
 */
@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    /**
     * Adds a new category to database.
     *
     * @param categoryName
     *            - name of category to be added. 
     *
     * @throws ZomatoException when adding category fails.
     */
    public void addCategory(String categoryName) throws ZomatoException {
        Category category = new Category(categoryName);
        categoryDao.insert(category);
    }
    
    /**
     * Returns all the categories for the specified restaurant id.
     *
     * @param restaurantId
     *            - restaurant Id whose categories is required 
     *
     * @return all catgories belonging to the restaurant
     *
     * @throws ZomatoException when getting categories for restaurant fails.
     */
    public Set getCategories(int restaurantId) throws ZomatoException {
        return categoryDao.getCategories(restaurantId);
    }
    
    /**
     * Get all the categories from the database.
     *
     * @return all food catgories belonging to the restaurant
     *
     * @throws ZomatoException when getting all categories fail.
     */
    public Set getAllCategories() throws ZomatoException {
        return categoryDao.getAllCategories();
    }
    
    /**
     * Removes the category from the database
     *
     * @param id
     *            - id of category
     *
     * @throws ZomatoException when removing category fails.
     */
    public void removeCategory(int id) throws ZomatoException {
        categoryDao.delete(id);
    }
    
    /**
     * Gets the Category from database based on the id.
     *
     * @param id
     *            - id of the category.
     *
     * @return category specified by the id.
     *
     * @throws ZomatoException when getting category by id fails.
     */
    public Category getCategoryById(int id) throws ZomatoException {
        return categoryDao.getCategoryById(id);
    }
}
