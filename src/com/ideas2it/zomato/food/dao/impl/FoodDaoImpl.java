package com.ideas2it.zomato.food.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.food.dao.FoodDao;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Performs insert, update, delete, and read funcionalities for food.
 */
@Repository("foodDao")
public class FoodDaoImpl implements FoodDao {

    private SessionFactory sessionFactory;
    
    /**
     * Inserts a new food into the database.
     *
     * @param food
     *            - food object about to be inserted.
     *
     * @throws ZomatoException when inserting food fails.
     */
    public void insert(Food food) throws ZomatoException {
        sessionFactory.getCurrentSession().save(food);
    }
    
    /**
     * Deletes the food from the database specified by id.
     *
     * @param id
     *            - id of food to be deleted.
     *
     * @throws ZomatoException when deleting food fails.
     */
    public void delete(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Food food = (Food)session.get(Food.class, id);
        session.delete(food);
    }
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     *
     * @throws ZomatoException when getting foods fail.
     */
    public Set getFoods() throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createQuery("FROM Food").list());
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Gets the Food from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return food specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    public Food getFoodById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Food) session.get(Food.class, id);
    }
}
