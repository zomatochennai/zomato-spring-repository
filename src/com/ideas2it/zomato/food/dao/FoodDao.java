package com.ideas2it.zomato.food.dao;

import java.util.Set;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Food dao class.
 */
public interface FoodDao {

   /**
     * Inserts a new food into the database.
     *
     * @param food
     *            - food object about to be inserted.
     *
     * @throws ZomatoException when inserting food fails.
     */
    void insert(Food food) throws ZomatoException;
    
    /**
     * Deletes the food from the database specified by id.
     *
     * @param id
     *            - id of food to be deleted.
     *
     * @throws ZomatoException when deleting food fails.
     */
    void delete(int id) throws ZomatoException;
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     *
     * @throws ZomatoException when getting foods fail.
     */
    Set getFoods() throws ZomatoException; 
    
    /**
     * Gets the Food from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return food specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    Food getFoodById(int id) throws ZomatoException;
}
