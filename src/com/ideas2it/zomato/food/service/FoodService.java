package com.ideas2it.zomato.food.service;

import java.util.Set;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for food service class.
 */
public interface FoodService {

    /**
     * Adds a new food to the database.
     *
     * @param foodName
     *            - name of food to be added.
     *
     * @throws ZomatoException when adding food fails.
     */
    public void addFood(String foodName) throws ZomatoException;
    
    /**
     * Removes the food specified by id.
     *
     * @param id
     *            - id of food .
     *
     * @throws ZomatoException when removing food fails.
     */
    void removeFood(int id) throws ZomatoException;
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     *
     * @throws ZomatoException when getting foods fail.
     */
    Set getFoods() throws ZomatoException;
    
    /**
     * Gets the food from the database specified by id.
     *
     * @param id
     *            - id of food .
     *
     * @return the food specified by id.
     *
     * @throws ZomatoException when getting foods fail.
     */
    Food getFoodById(int id) throws ZomatoException;
}
