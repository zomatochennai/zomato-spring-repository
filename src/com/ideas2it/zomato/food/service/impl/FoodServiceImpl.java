package com.ideas2it.zomato.food.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.food.service.FoodService;
import com.ideas2it.zomato.food.dao.FoodDao;
import com.ideas2it.zomato.food.dao.impl.FoodDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for food.
 */
@Service("foodService")
@Transactional
public class FoodServiceImpl implements FoodService {

    @Autowired
    private FoodDao foodDao;

    /**
     * Adds a new food to the database.
     *
     * @param foodName
     *            - name of food to be added .
     *
     * @throws ZomatoException when adding food fails.
     */
    public void addFood(String foodName) throws ZomatoException {
        Food food = new Food(foodName);
        foodDao.insert(food);
    }
    
    /**
     * Removes the food specified by id.
     *
     * @param id
     *            - id of food .
     *
     * @throws ZomatoException when removing food fails.
     */
    public void removeFood(int id) throws ZomatoException {
        foodDao.delete(id);
    }
    
    /**
     * Gets all the foods from the database.
     *
     * @return all foods in the database.
     *
     * @throws ZomatoException when getting foods fail.
     */
    public Set getFoods() throws ZomatoException {
         return foodDao.getFoods();
    }
    
    /**
     * Gets the food from the database specified by id.
     *
     * @return food specified by id.
     *
     * @throws ZomatoException when getting food fails.
     */
    public Food getFoodById(int id) throws ZomatoException {
         return foodDao.getFoodById(id);
    }
}
