package com.ideas2it.zomato.food;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;

/**
 * Individual food for the menuitem. It defines 'types of food' that can be
 * added as menu item. 
 */
public class Food {

    private int id;
    private String name;
    private Set<MenuItem> menuItems;
    
    public Food() {
    
    }
    
    public Food(String name) {
        this.name = name;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setMenuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
    
    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }
}
