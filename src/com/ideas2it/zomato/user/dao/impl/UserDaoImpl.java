package com.ideas2it.zomato.user.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.HibernateTemplate;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.dao.UserDao;

/**
 * Performs insert, update, and delete funcionalities for user model. This 
 * class also performs read operation by specifying any particular user field.
 * Hibernate API is implemented to perform relational mapping between java  
 * objects and the database.
 */
@Repository ("userDao") 
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    /**
     * <p>
     * Inserts a new user into the database.
     * </p>
     *
     * @param user
     *            - user object about to be inserted.
     *
     * @return    - true or false based on user insertion.
     *
     * @throws ZomatoException when user insertion fails.
     */
    public void insert(User user) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param user
     *            - user object to be updated.
     *
     * @throws ZomatoException when user update fails.
     */
    public void update(User user) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }
    
    /**
     * <p>
     * Gets a user from database based on email.
     * </p>
     *
     * @param email
     *            - email of the user.
     *
     * @return user specified by email.
     *
     * @throws ZomatoException if getting user by email fails.
     */
    public User getUserByEmail(String email) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));
        return (User) criteria.uniqueResult();
    }
    
    /**
     * <p>
     * Gets a user from database based on id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return user specified by id.
     *
     * @throws ZomatoException if getting user by id fails.
     */
    public User getUserById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Restrictions.eq("id", id));
        return (User) criteria.uniqueResult();
    }
    
    /**
     * <p>
     * Deletes the user from the database specified by id.
     * </p>
     *
     * @param id
     *            - id of user to be deleted.
     *
     * @throws ZomatoException when user deletion fails.
     */
    public void delete(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.get(User.class, id);
        session.delete(user);
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
