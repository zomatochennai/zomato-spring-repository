package com.ideas2it.zomato.user.dao;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.address.Address;

/**
 * Specifies how the user model can interact with the database. This interface
 * should be implemented where interaction with database for user model 
 * is required.
 */
public interface UserDao {

    /**
     * <p>
     * Inserts a new user into the database.
     * </p>
     *
     * @param user
     *            - user object about to be inserted.
     *
     * @return    - true or false based on user insertion.
     *
     * @throws ZomatoException when user insertion fails.
     */
    void insert(User user) throws ZomatoException;
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param user
     *            - user object to be updated.
     *
     * @throws ZomatoException when user update fails.
     */
    void update(User user) throws ZomatoException;
    
    /**
     * <p>
     * Gets a user from database based on email.
     * </p>
     *
     * @param email
     *            - email of the user.
     *
     * @return user specified by email. Returns empty user if email
     *         does not exist.
     *
     * @throws ZomatoException if getting user by email fails.
     */
    User getUserByEmail(String email) throws ZomatoException; 
    
    /**
     * <p>
     * Gets a user from database based on id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return user specified by id. Returns empty user if id does not exist.
     *
     * @throws ZomatoException if getting user by id fails.
     */
    User getUserById(int id) throws ZomatoException;
    
    /**
     * <p>
     * Deletes the user from the database specified by id.
     * </p>
     *
     * @param id
     *            - id of user to be deleted.
     *
     * @throws ZomatoException when user deletion fails.
     */
    void delete(int id) throws ZomatoException;
}
