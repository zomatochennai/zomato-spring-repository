package com.ideas2it.zomato.user;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * User can login to the application using email and password.
 * User can order food and review Restaurants.
 */
public class User {

    private int id;
    private String firstName;
    private String lastName;
    private long mobile;
    private String email;
    private String password;
    private boolean onlineStatus;
    private String joinDate;
    private String lastLoginDate;
    private String petName;
    private String bicycleName;
    private Role role;
    private Set<Address> addresses = new HashSet<>();
    private Restaurant restaurant;
    private Set<Cart> carts;
    
    public User() {
    
    }
    
    public User(String firstName, String lastName, long mobile, String email, 
                String password, String petName, String bicycleName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.petName = petName;
        this.bicycleName = bicycleName;   
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setMobile(long mobile) {
        this.mobile = mobile;
    }
    
    public long getMobile() {
        return mobile;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setOnlineStatus(boolean onlineStatus) {
        this.onlineStatus = onlineStatus;
    }
    
    public Boolean getOnlineStatus() {
        return onlineStatus;
    }
    
    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }
    
    public String getJoinDate() {
        return joinDate;
    }
    
    public void setLastLoginDate(String lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
    
    public String getLastLoginDate() {
        return lastLoginDate;
    }
    
    public void setPetName(String petName) {
        this.petName = petName;
    }
    
    public String getPetName() {
        return petName;
    }
    
    public void setBicycleName(String bicycleName) {
        this.bicycleName = bicycleName;
    }
    
    public String getBicycleName() {
        return bicycleName;
    }
    
    public void setRole(Role role) {
        this.role = role;
    }
    
    public Role getRole() {
        return role;
    }
    
    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
    
    public Set<Address> getAddresses() {
        return addresses;
    }
    
    public void addAddress(Address address) {
        addresses.add(address);
    }
    
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }
    
    public void setCarts(Set<Cart> carts) {
        this.carts = carts;
    }
    
    public Set<Cart> getCarts() {
        return carts;
    }
    
    public void addCart(Cart cart) {
        carts.add(cart);
    }
    
    /**
     * <p>
     * Returns the user's active cart.
     * </p>
     *
     * @return Cart that is currently active.
     */
    public Cart getActiveCart() {
        Cart activeCart = new Cart();
        for (Cart cart : carts) {
            if (cart.isActive()) {
                activeCart = cart;
                break;
            }
        }
        return activeCart;
    }
}
