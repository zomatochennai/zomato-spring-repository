package com.ideas2it.zomato.user.service;

import java.util.Set;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.address.Address;

/**
 * Contains all the required logical operations for user model.
 */
public interface UserService {

    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param user
     *            - user to be registered .
     *
     * @param address
     *            - address of the user to be added.
     *
     * @param roleId
     *            - id of the user role.
     *
     * @return true or false based on user registration.
     *
     * @throws ZomatoException when user registration fails.
     */
    boolean register(User user, Address address, int roleId) 
                     throws ZomatoException;
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param userId
     *            - id of the user.
     *
     * @param address
     *            - new address to be added.
     *
     * @return true or false based on address assigning.
     *
     * @throws ZomatoException if adding address to user fails.
     */
    boolean addAddress(int userId, Address address) throws ZomatoException;
    
    /**
     * <p>
     * Logs in the user to the application.
     * If the entered email does not exist in the database or if the entered
     * password does not match the email, login fails.
     * </p> 
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user .
     *
     * @return User that is logged in.
     *
     * @throws ZomatoException when user login fails.
     */
    User login(String email, String password) throws ZomatoException;
    
    /**
     * <p>
     * Logs out the user from the application based on id.
     * </p>
     *
     * @param userId
     *            - id of the user.
     *
     * @return true or false based on user logout.
     *
     * @throws ZomatoException when user logout fails.
     */
    boolean logout(int id) throws ZomatoException;
    
    /**
     * <p>
     * Enables user to reset old password after checking the security questions
     * petName and bicycleName.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user .
     *
     * @return true or false based on password reset.
     *
     * @throws ZomatoException when reset password operation fails.
     */
    boolean resetPassword(String email, String password, String petName, 
                     String bicycleName) throws ZomatoException;
    
    /**
     * <p>
     * Enables user to change his existing password.
     * </p>
     *
     * @param oldPassword
     *            - oldPassword of the user.
     *
     * @param newPassword
     *            - newPassword entered by the user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return true or false based on password change.
     *
     * @throws ZomatoException when password change fails.
     */
    public boolean changePassword(String oldPassword, String newPassword, 
                                  int userId) throws ZomatoException;
    
    /**
     * <p>
     * Returns the user specified by the id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @param user
     *            - user for the specified id .
     *
     * @throws ZomatoException if getting user by id fails.
     */
    User getUserById(int id) throws ZomatoException;
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param userId
     *            - id of user to be updated.
     *
     * @param firstName
     *            - updated firstName of user.
     *
     * @param lastName
     *            - updated lastName of user.
     *
     * @param mobile
     *            - updated mobile of user.
     *
     * @param email
     *            - updated email of user.
     *
     * @return the updated user.
     *
     * @throws ZomatoException when user update fails.
     */
    User updateUser(int userId, String firstName, String lastName, 
                           long mobile, String email) throws ZomatoException;
}
