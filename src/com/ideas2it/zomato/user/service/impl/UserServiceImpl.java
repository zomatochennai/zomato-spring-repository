package com.ideas2it.zomato.user.service.impl;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.common.StringUtil;
import com.ideas2it.zomato.role.Role;
import com.ideas2it.zomato.role.service.RoleService;
import com.ideas2it.zomato.role.service.impl.RoleServiceImpl;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.dao.UserDao;
import com.ideas2it.zomato.user.dao.impl.UserDaoImpl;
import com.ideas2it.zomato.user.User;

/**
 * Performs all user related logical operations. Implements spring transaction
 * annotations to manage transactions for the dao. 
 */
@Service ("userService")
@Transactional
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleService roleService;

    /**
     * <p>
     * Registers a new user to the application, along with a address and role.
     * </p>
     *
     * @param user
     *            - user to be registered.
     *
     * @param address
     *            - address of the user to be added.
     *
     * @param roleId
     *            - id of the user role.
     *
     * @return true or false based on user registration.
     *
     * @throws ZomatoException when user registration fails.
     */
    public boolean register(User user, Address address, int roleId) 
                            throws ZomatoException {
        boolean success = false;
        if (null == userDao.getUserByEmail(user.getEmail())) {
            if (StringUtil.validateEmail(user.getEmail()) &&
                StringUtil.validateMobileNumber(user.getMobile()) &&
                StringUtil.validatePassword(user.getPassword()) ) { 
                address.setUser(user);
                user.addAddress(address);
                user.setJoinDate(StringUtil.getDateTime());
                Role role = roleService.getRoleById(roleId);
                user.setRole(role);
                userDao.insert(user);
                success = true;
            }
        }
        return success;
    }
    
    /**
     * <p>
     * Adds a new address to the user.
     * </p>
     *
     * @param userId
     *            - id of the user.
     *
     * @param address
     *            - new address to be added.
     *
     * @return true or false based on address assigning.
     *
     * @throws ZomatoException if adding address to user fails.
     */ 
    public boolean addAddress(int userId, Address address) 
                              throws ZomatoException {
        boolean success = false;
        User user = userDao.getUserById(userId);
        if (null != user) {
            user.addAddress(address);
            userDao.update(user);
            success = true;
        }
        return success;
    }
    
    /**
     * <p>
     * Logs in the user to the application.
     * If the entered email does not exist in the database or if the entered
     * password does not match the email, login fails.
     * </p> 
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user .
     *
     * @return User that is logged in. If login fails, empty user is returned. 
     *
     * @throws ZomatoException when user login fails.
     */
    public User login(String email, String password) throws ZomatoException {
        User user = null;
        if (StringUtil.validateEmail(email) && 
            StringUtil.validatePassword(password)) {
            user = userDao.getUserByEmail(email);
            if (null != user && user.getPassword().equals(password)) {
                user.setOnlineStatus(true);
                user.setLastLoginDate(StringUtil.getDateTime());
                userDao.update(user);
            }
        }
        return user;
    }
    
    /**
     * <p>
     * Logs out the user from the application based on id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return true or false based on successful logout operation.
     *
     * @throws ZomatoException when user logout fails.
     */
    public boolean logout(int id) throws ZomatoException {
        boolean success = false;
        User user = userDao.getUserById(id);
        if (null != user) {
            user.setOnlineStatus(false);
            userDao.update(user);
            success = true;
        }
        return success;
    }
    
    /**
     * <p>
     * Enables user to reset old password after checking the security questions
     * petName and bicycleName.
     * </p>
     *
     * @param email
     *            - email id of the user.
     *
     * @param password
     *            - password of the user.
     *
     * @param petName
     *            - pet name of the user.
     *
     * @param bicycleName
     *            - bicycle name of the user .
     *
     * @return true or false based on password reset.
     *
     * @throws ZomatoException when reset password operation fails.
     */
    public boolean resetPassword(String email, String password, String petName, 
                               String bicycleName) throws ZomatoException {
        boolean success = false;
        User user = userDao.getUserByEmail(email);
        if (null != user && StringUtil.validatePassword(password)) {
            if ((user.getPetName().equals(petName)) &&
                                (user.getBicycleName().equals(bicycleName))) {
                user.setPassword(password);
                userDao.update(user);
                success = true;
            }
        }
        return success;
    }
    
    /**
     * <p>
     * Enables user to change his existing password.
     * </p>
     *
     * @param oldPassword
     *            - oldPassword of the user.
     *
     * @param newPassword
     *            - newPassword entered by the user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return true or false based on password change.
     *
     * @throws ZomatoException when password change fails.
     */
    public boolean changePassword(String oldPassword, String newPassword, 
                                  int userId) throws ZomatoException {
        boolean success = false;
        User user = userDao.getUserById(userId);
        if (null != user && StringUtil.validatePassword(newPassword)) {
            if ((user.getPassword().equals(oldPassword)) ) {
                user.setPassword(newPassword);
                userDao.update(user);
                success = true;
            }
        }
        return success;
    }
    
    /**
     * <p>
     * Returns the user specified by the id.
     * </p>
     *
     * @param id
     *            - id of the user.
     *
     * @return user for the specified id.
     *
     * @throws ZomatoException if getting user by id fails.
     */
    public User getUserById(int id) throws ZomatoException {
        return userDao.getUserById(id);
    }
    
    /**
     * <p>
     * Updates the user in the database.
     * </p>
     *
     * @param userId
     *            - id of user to be updated.
     *
     * @param firstName
     *            - updated firstName of user.
     *
     * @param lastName
     *            - updated lastName of user.
     *
     * @param mobile
     *            - updated mobile of user.
     *
     * @param email
     *            - updated email of user.
     *
     * @return the updated user.
     *
     * @throws ZomatoException when user update fails.
     */
    public User updateUser(int userId, String firstName, String lastName, 
                           long mobile, String email) throws ZomatoException {
        User user = userDao.getUserById(userId);
        if (null != user && (StringUtil.validateEmail(email)) &&
                             StringUtil.validateMobileNumber(mobile)) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMobile(mobile);
            user.setEmail(email);
            userDao.update(user);
        }
        return user;
    }
}
