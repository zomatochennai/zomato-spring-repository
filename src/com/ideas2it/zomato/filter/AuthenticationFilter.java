package com.ideas2it.zomato.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
 
/**
 * User session authentication filter implementation.
 */
public class AuthenticationFilter implements Filter {

    private ArrayList urlList;
    private static final Logger logger = Logger.getLogger(AuthenticationFilter.class);
    
    /**
     * Initializes the variables for filter operation.
     *
     * @param config
     *            - configuration object for the filter.
     */
    public void init(FilterConfig config) throws ServletException {
        // Read the URLs to be avoided for authentication check (From web.xml)
        logger.debug("Authentication filter initialized");
        String urls = config.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");
        urlList = new ArrayList();
        while (token.hasMoreTokens()) {
            urlList.add(token.nextToken());
        }
    }
    
    /**
     * Does session authentication operation. Redirects to Welcome page 
     * if authentication fails.
     *
     * @param req
     *            - Servlet request object.
     *
     * @param res
     *            - Servlet response object.
     *
     * @param chain
     *            - FilterChain object.
     */
    public void doFilter(ServletRequest req, ServletResponse res,
                       FilterChain chain) throws IOException, ServletException {
        logger.debug("Authentication: inside dofilter");
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String url = request.getServletPath();
        logger.debug("Authentication: request path url " + url);
        boolean allowedRequest = false;
        String strURL = "";

        // To check if the url can be excluded or not
        for (int i = 0; i < urlList.size(); i++) {
            strURL = urlList.get(i).toString();
            if (url.startsWith(strURL)) {
                allowedRequest = true;
            }
        }
        HttpSession session = request.getSession(false);

        if (!allowedRequest) {
            logger.debug("Authentication: inside allowed request");
            if (null == session || (Integer)session.getAttribute("userId") == null 
                           || (Integer)session.getAttribute("userId") == 0) {
                logger.debug("session is null");
                // Forward the control to Welcome.jsp if authentication fails
                request.setAttribute("message", "Session Expired! Please try again!");
                request.getRequestDispatcher("/view/Welcome.jsp").forward(request,
                response);
            } else {
                chain.doFilter(req, res);
            }
        } else {
            chain.doFilter(req, res);
        }
    }
    
    /**
     * No cleanup operation required.
     */
    public void destroy() {
    
    }
}
