package com.ideas2it.zomato.common;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Date;

/**
 * Utility class implementation for validation purpose.
 */
public class StringUtil {
    
    /**
     * Validates the email id entered by user.
     *
     * @param email
     *            - email id to be validated .
     *
     * @return true if entered email is valid.
     *         false if entered email is not valid.
     */
    public static boolean validateEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    /**
     * Validates the mobile number by checking the number of digits.
     *
     * @param number
     *            - mobile number to be validated .
     *
     * @return true if entered mobile number is valid.
     *         false if entered mobile number is not valid.
     */
    public static boolean validateMobileNumber(long number) {
        boolean valid = false;
        int validLength = 10;
        int length = (int)(Math.log10(number)+1);
        if (length == validLength) {
            valid = true;
        }
        return valid;
    }
    
    /**
     * Validates the mobile number by checking the number of digits.
     *
     * @param number
     *            - mobile number to be validated .
     *
     * @return true if entered mobile number is valid.
     *         false if entered mobile number is not valid.
     */
    public static boolean validatePassword(String password) {
        boolean valid = false;
        int validLength = 10;
        if (password.length() >= 8 && password.length() <= 16) {
            valid = true;
        }
        return valid;
    }
    
    /**
     * Sets the current date and time.
     *
     * @return current date time.
     */
    public static String getDateTime() {
        SimpleDateFormat simpleDateFormat = 
                                    new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return (simpleDateFormat.format(new Date()));
    }
}
