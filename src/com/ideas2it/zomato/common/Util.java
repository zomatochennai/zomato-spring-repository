package com.ideas2it.zomato.common;

import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Date;

/**
 * Utility class implementation for validation purpose.
 */
public class Util {
    
    /**
     * Validates the email id entered by user.
     *
     * @param email
     *            - email id to be validated .
     *
     * @return true if entered email is valid.
     *         false if entered email is not valid.
     */
    public static boolean validateEmail(String email) {
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    /**
     * Validates the mobile number by checking the number of digits.
     *
     * @param number
     *            - mobile number to be validated .
     *
     * @return true if entered mobile number is valid.
     *         false if entered mobile number is not valid.
     */
    public static boolean validateMobileNumber(long number) {
        boolean isValid = false;
        int validLength = 10;
        long length = (long)(Math.log10(number)+1);
        if (length == validLength) {
            isValid = true;
        }
        return isValid;
    }
    
    /**
     * Sets the current date and time.
     *
     * @return current date time.
     */
    public static String getDateTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = 
                                    new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return (simpleDateFormat.format(date));
    }
}
