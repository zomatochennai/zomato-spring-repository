package com.ideas2it.zomato.classification;

import java.util.Set;

import com.ideas2it.zomato.restaurant.Restaurant;

/**
 * Classification type for the restaurant. 
 */
public class Classification {

    private int id;
    private String name;
    private Set<Restaurant> restaurants;
    
    public Classification() {
    
    }
    
    public Classification(String name) {
        this.name = name;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setRestaurants(Set<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }
    
    public Set<Restaurant> getRestaurants() {
        return restaurants;
    }
}
