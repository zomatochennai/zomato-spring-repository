package com.ideas2it.zomato.classification.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.dao.ClassificationDao;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Performs all the database related funcionalities for Classification object.
 */
@Repository("classificationDao")
public class ClassificationDaoImpl implements ClassificationDao {

    private SessionFactory sessionFactory;

    /**
     * Inserts a new classification into the database.
     *
     * @param classification
     *            - classification object about to be inserted.
     *
     * @throws ZomatoException when inserting classification fails.
     */
    public void insert(Classification classification) throws ZomatoException {
        sessionFactory.getCurrentSession().save(classification); 
    }
    
    /**
     * Returns all the classifications from the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    public Set getClassifications() throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createQuery("FROM Classification").list());
    }
        
    /**
     * Deletes the classification from the database specified by id.
     *
     * @param id
     *            - id of classification to be deleted.
     *
     * @throws ZomatoException when deleting classification fails.
     */
    public void deleteClassification(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Classification classification = (Classification)session
                                            .get(Classification.class, id);
        session.delete(classification);
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    public Classification getClassificationById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Classification) session.get(Classification.class, id);
    }
}
