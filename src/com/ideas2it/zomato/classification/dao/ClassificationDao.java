package com.ideas2it.zomato.classification.dao;

import java.util.Set;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Classification dao class
 */
public interface ClassificationDao {

    /**
     * Inserts a new classification into the database.
     *
     * @param classification
     *            - classification object about to be inserted.
     *
     * @throws ZomatoException when inserting classification fails.
     */
    void insert(Classification classification) throws ZomatoException;
    
    /**
     * Returns all the classifications from the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    Set getClassifications() throws ZomatoException;
    
    /**
     * Deletes the classification from the database specified by id.
     *
     * @param id
     *            - id of classification to be deleted.
     *
     * @throws ZomatoException when deleting classification fails.
     */
    void deleteClassification(int id) throws ZomatoException;
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    Classification getClassificationById(int id) throws ZomatoException;
}
