package com.ideas2it.zomato.classification.service;

import java.util.Set;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Classification service class.
 */
public interface ClassificationService {

    /**
     * Adds a new classification to the database.
     *
     * @param classificationName
     *            - name of new classification to be added .
     *
     * @throws ZomatoException when adding classification fails.
     */
    void addClassification(String classificationName) throws ZomatoException;
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    Set getClassifications() throws ZomatoException;
    
    /**
     * Removes the classification from database specified by id.
     *
     * @param id
     *            - id of classification .
     *
     * @throws ZomatoException when removing classification fails.
     */
    void removeClassification(int id) throws ZomatoException;
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    Classification getClassificationById(int classificationId) 
                                         throws ZomatoException;
}
