package com.ideas2it.zomato.classification.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.service.ClassificationService;
import com.ideas2it.zomato.classification.dao.ClassificationDao;
import com.ideas2it.zomato.classification.dao.impl.ClassificationDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for classification.
 */
@Service("classificationService")
@Transactional
public class ClassificationServiceImpl implements ClassificationService {

    @Autowired
    private ClassificationDao classificationDao;

    /**
     * Adds a new classification to the database.
     *
     * @param classificationName
     *            - name of new classification to be added .
     *
     * @throws ZomatoException when adding classification fails.
     */
    public void addClassification(String classificationName) 
                                  throws ZomatoException {
        Classification classification = new Classification(classificationName);
        classificationDao.insert(classification);
    }
    
    /**
     * Removes the classification from database specified by id.
     *
     * @param id
     *            - id of classification to be removed.
     *
     * @throws ZomatoException when removing classification fails.
     */
    public void removeClassification(int id) throws ZomatoException {
        classificationDao.deleteClassification(id);
    }
    
    /**
     * Returns all the classifications in the database.
     *
     * @return set of classifications.
     *
     * @throws ZomatoException when getting classifications fail.
     */
    public Set getClassifications() throws ZomatoException {
        return classificationDao.getClassifications();
    }
    
    /**
     * Returns the classification from the database specified by id.
     *
     * @return classification specified by id.
     *
     * @throws ZomatoException when getting classification by id fail.
     */
    public Classification getClassificationById(int classificationId) 
                                                throws ZomatoException {
        return classificationDao.getClassificationById(classificationId);
    }
}
