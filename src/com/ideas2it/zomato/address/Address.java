package com.ideas2it.zomato.address;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.user.User;

/**
 * Holds the complete address and pincode for user or restaurant.
 */
public class Address {

    private int id;
    private String addressLineOne;
    private String addressLineTwo;
    private String city;
    private String state;
    private int pincode;
    private User user;
    private Restaurant restaurant;
    
    public Address() {
    
    }
    
    public Address(String addressLineOne, String addressLineTwo, String city,
                   String state, int pincode) {
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }
    
    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }
    
    public String getAddressLineTwo() {
        return addressLineTwo;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return city;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    public String getState() {
        return state;
    }
    
    public void setPincode(int pincode) {
        this.pincode = pincode;
    }
    
    public int getPincode() {
        return pincode;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }
}
