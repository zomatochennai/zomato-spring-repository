package com.ideas2it.zomato.order.service;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * Interface implementation for order service class.
 */
public interface OrderService {

    /**
     * Returns the order for the cart id.
     *
     * @param cartId
     *            - cart id for the required order .
     *
     * @return Order of the cart.
     *
     * @throws ZomatoException when getting order for cart fails.
     */
    Order getOrderByCartId(int cartId) throws ZomatoException;
    
    /**
     * Creates a new order for the payment.
     *
     * @param cartId
     *            - cart id for the required order.
     *
     * @throws ZomatoException when creating order fails.
     */
    void createOrder(int cartId) throws ZomatoException;
    
    /**
     * Confirms the order for the payment id.
     *
     * @param paymentId
     *            - payment id for the order.
     *
     * @return order that is confirmed.
     *
     * @throws ZomatoException if order confirmation fails.
     */
    Order confirmOrder(int paymentId) throws ZomatoException;
}
