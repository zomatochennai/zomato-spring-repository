package com.ideas2it.zomato.order.service.impl;

import java.util.Set;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.service.OrderService;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.order.dao.OrderDao;
import com.ideas2it.zomato.order.dao.impl.OrderDaoImpl;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.cart.service.impl.CartServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * Handles order related functionalities including order creation and 
 * confirmation.
 */
@Service ("orderService")
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private CartService cartService;
    
    /**.
     * Returns the order for the specified cart id.
     *
     * @param cartId
     *            - cart id of the required order.
     *
     * @return Order for the specified cart id.
     *
     * @throws ZomatoException when getting order using cart id fails.
     */
    public Order getOrderByCartId(int cartId) throws ZomatoException {
        return orderDao.getOrderByCartId(cartId);
    }
    
    /**
     * Creates a new order for the cart specified by id.
     *
     * @param cartId
     *            - cart id for the order to be created.
     *
     * @throws ZomatoException when creating order fails.
     */
    public void createOrder(int cartId) throws ZomatoException {
        Cart cart = cartService.getCartById(cartId);
        Order order = orderDao.getOrderByCartId(cartId);
        if (null == order) {
            order = new Order(cart);
        }
        cart.setOrder(order);
        order.setCost(cart.getCost());
        Set<OrderDetail> orderDetails = new HashSet<>();
        for (CartDetail cartDetail : cart.getCartDetails()) {
            OrderDetail orderDetail = new OrderDetail(
                                          cartDetail.getMenuItemName(),
                                          cartDetail.getQuantity(),
                                          cartDetail.getPrice());
            orderDetail.setOrder(order);
            orderDetails.add(orderDetail);
        }
        order.setOrderDetails(orderDetails);
        orderDao.insert(order);
    }
    
    /**
     * Confirms the order for specified payment id.
     *
     * @param paymentId
     *            - payment id for the order.
     *
     * @return order that is confirmed.
     *
     * @throws ZomatoException if order confirmation fails.
     */
    public Order confirmOrder(int paymentId) throws ZomatoException {
        Order order = orderDao.getOrderByPaymentId(paymentId);
        order.setPaymentDone(true);
        orderDao.update(order);
        return order;
    }
}
