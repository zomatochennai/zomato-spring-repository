package com.ideas2it.zomato.order.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.order.dao.OrderDao;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This class handles access between Food Order services and hibernate.
 */
@Repository ("orderDao")
public class OrderDaoImpl implements OrderDao {

    private SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class); 
    
    /**
     * Returns the order for the specified user.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @return order object for the id.
     *
     * @throws ZomatoException when getting order by user id fails.
     */
    public Order getOrderByCartId(int cartId) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Order)session.createCriteria(Order.class).createCriteria("cart")
                            .add( Restrictions.eq("id", cartId)).uniqueResult();
    }
    
    /**
     * Inserts the order into the database.
     *
     * @param order
     *            - order to be inserted.
     *
     * @throws ZomatoException when inserting order fails.
     */
    public void insert(Order order) throws ZomatoException {
        sessionFactory.getCurrentSession().saveOrUpdate(order); 
    }
    
    /**
     * Update the order to the database.
     *
     * @param order
     *            - order to be updated.
     *
     * @throws ZomatoException when updating order fails.
     */
    public void update(Order order) throws ZomatoException {
        sessionFactory.getCurrentSession().update(order); 
    }
    
    /**
     * Returns the order for the specified payment.
     *
     * @param paymentId
     *            - id of the payment.
     *
     * @return order for the payment id.
     *
     * @throws ZomatoException when getting order by payment id fails.
     */
    public Order getOrderByPaymentId(int paymentId) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Order)session.createCriteria(Order.class)
                             .createCriteria("payment").add( Restrictions
                             .eq("id", paymentId)).uniqueResult();
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
