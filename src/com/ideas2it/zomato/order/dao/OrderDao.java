package com.ideas2it.zomato.order.dao;

import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.orderDetail.OrderDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This class handles access between Order services and hibernate .
 */
public interface OrderDao {
    
    /**
     * Returns the order for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return order object for the id.
     *
     * @throws ZomatoException when getting order by id fails.
     */
    Order getOrderByCartId(int cartId) throws ZomatoException;
    
    /**
     * Inserts the order into the database.
     *
     * @param order
     *            - order to be inserted.
     *
     * @throws ZomatoException when inserting order fails.
     */
    void insert(Order order) throws ZomatoException;
    
    /**
     * Update the order to the database.
     *
     * @param order
     *            - order to be updated.
     *
     * @throws ZomatoException when updating order fails.
     */
    void update(Order order) throws ZomatoException;
    
    /**
     * Returns the order for the specified payment.
     *
     * @param paymentId
     *            - id of the payment.
     *
     * @return order for the payment id.
     *
     * @throws ZomatoException when getting order by payment id fails.
     */
    Order getOrderByPaymentId(int paymentId) throws ZomatoException;
}
