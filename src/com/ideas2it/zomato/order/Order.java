package com.ideas2it.zomato.order;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.payment.Payment;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.orderDetail.OrderDetail;

/**
 * This is model class for Order. An order is created when user checks out
 * cart and order is confirmed once payment is confirmed.
 */
public class Order {

    private int id;
    private Payment payment;
    private Set<OrderDetail> orderDetails = new HashSet<>();
    private boolean paymentDone;
    private float cost; 
    private Cart cart;
    
    public Order() {
    
    }
    
    public Order(Cart cart) {
        this.cart = cart;
        this.cost = cart.getCost();
    }
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setPayment(Payment payment) {
        this.payment = payment;
    }
    
    public Payment getPayment() {
        return payment;
    }
    
    public void setOrderDetails(Set<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    
    public Set<OrderDetail> getOrderDetails() {
        return orderDetails;
    }
    
    public void addOrderDetail(OrderDetail orderDetail) {
        orderDetails.add(orderDetail);
    }
    
    public void setPaymentDone(boolean paymentDone) {
        this.paymentDone = paymentDone;
    }
    
    public boolean isPaymentDone() {
        return paymentDone;
    }
    
    public void setCost(float cost) {
        this.cost = cost;
    }
    
    public float getCost() {
        return cost;
    }
    
    public void setCart(Cart cart) {
        this.cart = cart;
    }
    
    public Cart getCart() {
        return cart;
    }
}
