package com.ideas2it.zomato.restaurant;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.classification.Classification;

/**
 * Restaurant contains menuitems, which can be ordered by User.
 */
public class Restaurant {

    private int id;
    private String name;
    private String description;
    private String startingTime;
    private String closingTime;
    private boolean homeDelivery;
    private User user;
    private Set<Classification> classifications = new HashSet<>();
    private Set<MenuItem> menuItems = new HashSet<>();
    private Address address;
    
    public Restaurant() {
    
    }
    
    public Restaurant(String name, String description, String startingTime,
                      String closingTime) {
        this.name = name;
        this.description = description;
        this.startingTime = startingTime;
        this.closingTime = closingTime;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setStartingTime(String startingTime) {
        this.startingTime = startingTime;
    }
    
    public String getStartingTime() {
        return startingTime;
    }
    
    public void setClosingTime(String closingTime) {
        this.closingTime = closingTime;
    }
    
    public String getClosingTime() {
        return closingTime;
    }
    
    public void setHomeDelivery(boolean homeDelivery) {
        this.homeDelivery = homeDelivery;
    }
    
    public boolean getHomeDelivery() {
        return homeDelivery;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setClassifications(Set<Classification> classifications) {
        this.classifications = classifications;
    }
    
    public Set<Classification> getClassifications() {
        return classifications;
    }
    
    public void addClassification(Classification classification) {
        classifications.add(classification);
    }
    
    public void setAddress(Address address) {
        this.address = address;
    }
    
    public Address getAddress() {
        return address;
    }
    
    public void setMenuItems(Set<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }
    
    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }
    
    public void removeClassification(int id) {
        for (Classification classification : classifications) {
            if (classification.getId() == id) {
                classifications.remove(classification);
                break;
            }
        }
    }
    
    public void deleteMenuItem(int menuItemId) {
        for (MenuItem menuItem : menuItems) {
            if (menuItem.getId() == menuItemId) {
                menuItems.remove(menuItem);
                break;
            }
        }
    }
}
