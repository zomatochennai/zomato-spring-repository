package com.ideas2it.zomato.restaurant.service;

import java.util.Set;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Restaurant service class.
 */
public interface RestaurantService {

    /**
     * Registers a new restaurant.
     *
     * @param restaurant
     *            - restaurant object to be registered .
     *
     * @param address
     *            - address of the restaurant.
     *
     * @param userId
     *            - id of restaurant's vendor.
     *
     * @return true or false based on restaurant registration.
     *
     * @throws ZomatoException when restaurant registration fails.
     */
    boolean registerRestaurant(Restaurant restaurant, Address address,
                               int userId) throws ZomatoException;
    
    /**
     * Returns all the restaurants for the specified classification id.
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fail.
     */
    Set getRestaurantsByClassificationId(int classificationId) 
                                         throws ZomatoException;
    
    /**
     * Deletes the restaurant specified by id.
     *
     * @param id
     *            - id of restaurant about to be deleted .
     *
     * @throws ZomatoException when deleting restaurant fails.
     */
    void deleteRestaurantById(int id) throws ZomatoException;
    
    /**
     * Deletes a menuItem from the restaurant.
     *
     * @param restaurantId
     *            - id of the restaurant.
     *
     * @param menuItemId
     *            - id of menuItem to be deleted.
     *
     * @throws ZomatoException when deleting menuItem fails.
     */
    void deleteMenuItem(int restaurantId, int menuItemId) 
                               throws ZomatoException;
    
    /**
     * Returns the restaurant specified by the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return Restaurant for the specified id.
     *
     * @throws ZomatoException when getting restaurant fails.
     */
    Restaurant getRestaurantById(int id) throws ZomatoException;
    
    /**
     * Updates the restaurant with address to the database.
     *
     * @param newRestaurant
     *            - new restaurant details to be updated.
     *
     * @param newAddress
     *            - new address details to be updated.
     *
     * @throws ZomatoException when updating restaurant fails.
     */
    Restaurant updateRestaurant(Restaurant newRestaurant, 
                                Address newAddress) throws ZomatoException;
    
    /**
     * Returns all the restaurants matching the specified name.
     *
     * @param restaurantName
     *            - name of restaurants to be fetched.
     *
     * @return set of restaurants for specified name.
     *
     * @throws ZomatoException when getting restaurants fail.
     */
    Set getRestaurantsByName(String restaurantName) throws ZomatoException;

    /**
     * Assigns a new classification to the specified restaurant.
     *
     * @param restaurantId
     *            - id of the restaurant.
     *
     * @param classificationIds
     *            - ids of classification to be assigned.
     *
     * @throws ZomatoException when assigning classifications fail.
     */
    void assignClassifications(int restaurantId, 
                          String[] classificationIds) throws ZomatoException;

    /**
     * Removes a classification from the restaurant.
     *
     * @param restaurantId
     *            - id of the restaurant.
     *
     * @param classificationId
     *            - id of classification to be removed.
     *
     * @throws ZomatoException when removing classification fails.
     */
    void removeRestaurantClassification(int restaurantId,
                                  int classificationId) throws ZomatoException;
}
