package com.ideas2it.zomato.restaurant.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.address.Address;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.classification.service.ClassificationService;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.restaurant.service.RestaurantService;
import com.ideas2it.zomato.restaurant.dao.RestaurantDao;
import com.ideas2it.zomato.restaurant.dao.impl.RestaurantDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for restaurant.
 */
@Service ("restaurantService")
@Transactional
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantDao restaurantDao;
    @Autowired
    private UserService userService;
    @Autowired
    private ClassificationService classificationService;

    /**
     * Registers a new restaurant.
     *
     * @param restaurant
     *            - restaurant to be registered .
     *
     * @param address
     *            - address of the restaurant.
     *
     * @param userId
     *            - id of restaurant's vendor.
     *
     * @return true or false based on restaurant registration.
     *
     * @throws ZomatoException when restaurant registration fails.
     */
    public boolean registerRestaurant(Restaurant restaurant, Address address,
                                      int userId) throws ZomatoException {
        boolean success = false;
        User user = userService.getUserById(userId);
        if (null == user.getRestaurant()) {
            restaurant.setUser(user);
            user.setRestaurant(restaurant);
            restaurant.setAddress(address);
            address.setRestaurant(restaurant);
            restaurantDao.insertRestaurant(restaurant);
            success = true;
        }
        return success;
    }
    
    /**
     * Returns the restaurants in the database for the classification id.
     *
     * @param classificationId
     *            - classification Id of restaurants to be fetched.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fail.
     */
    public Set getRestaurantsByClassificationId(int classificationId)
                                                throws ZomatoException {
        return restaurantDao.getRestaurants(classificationId);
    }
    
    /**
     * Deletes the restaurant specified by id.
     *
     * @param id
     *            - id of restaurant to be deleted .
     *
     * @throws ZomatoException when deleting restaurant fails.
     */
    public void deleteRestaurantById(int id) throws ZomatoException {
        restaurantDao.deleteRestaurant(id);
    }
    
    /**
     * Returns the restaurant specified by the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return Restaurant for the specified id
     *
     * @throws ZomatoException when getting restaurant fails.
     */
    public Restaurant getRestaurantById(int id) throws ZomatoException {
        return restaurantDao.getRestaurantById(id);
    }
    
    /**
     * Deletes a menuItem from the restaurant.
     *
     * @param id
     *            - id of menuItem to be deleted.
     *
     * @throws ZomatoException when deleting menuItem fails.
     */
    public void deleteMenuItem(int restaurantId, int menuItemId) 
                               throws ZomatoException {
        Restaurant restaurant = restaurantDao.getRestaurantById(restaurantId);
        restaurant.deleteMenuItem(menuItemId);
        restaurantDao.updateRestaurant(restaurant);
    }
    
    /**
     * Updates the restaurant with address to the database.
     *
     * @param newRestaurant
     *            - new restaurant details to be updated.
     *
     * @param newAddress
     *            - new address details to be updated.
     *
     * @throws ZomatoException when updating restaurant fails.
     */
    public Restaurant updateRestaurant(Restaurant newRestaurant, 
                               Address newAddress) throws ZomatoException {
        Restaurant restaurant = restaurantDao.getRestaurantById(newRestaurant
                                                                .getId());
        restaurant.setName(newRestaurant.getName());
        restaurant.setDescription(newRestaurant.getDescription());
        restaurant.setStartingTime(newRestaurant.getStartingTime());
        restaurant.setClosingTime(newRestaurant.getClosingTime());
        restaurant.setHomeDelivery(newRestaurant.getHomeDelivery());
        Address address = restaurant.getAddress();
        address.setAddressLineOne(newAddress.getAddressLineOne());
        address.setAddressLineTwo(newAddress.getAddressLineTwo());
        address.setCity(newAddress.getCity());
        address.setState(newAddress.getState());
        address.setPincode(newAddress.getPincode());
        restaurantDao.updateRestaurant(restaurant);
        return restaurant;
    }
    
    /**
     * Returns the restaurants in the database which matches the specified name.
     *
     * @param restaurantName
     *            - name of restaurants to be fetched.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fail.
     */
    public Set getRestaurantsByName(String restaurantName)
                                           throws ZomatoException {
        return restaurantDao.getRestaurantsByName(restaurantName);
    }
    
    /**
     * Assigns new classifications to the restaurant.
     *
     * @param restaurantId
     *            - id of restaurant .
     *
     * @param classificationIds
     *            - ids of classification that needs to be assigned.
     *
     * @throws ZomatoException when assigning classification fails.
     */
    public void assignClassifications(int restaurantId, 
                            String[] classificationIds) throws ZomatoException {
        Restaurant restaurant = restaurantDao.getRestaurantById(restaurantId);
        for (int i = 0; i < classificationIds.length; i++ ) {
            Classification classification = classificationService
                                            .getClassificationById(Integer
                                            .parseInt(classificationIds[i]));
            restaurant.addClassification(classification);
        }
        restaurantDao.updateRestaurant(restaurant);
    }
    
    /**
     * Removes a classification from the restaurant specified by id.
     *
     * @param restaurantId
     *            - id of restaurant.
     *
     * @param classificationId
     *            - id of classification that needs to be removed.
     *
     * @throws ZomatoException when assigning classification fails.
     */
    public void removeRestaurantClassification(int restaurantId,
                                  int classificationId) throws ZomatoException {
        Restaurant restaurant = restaurantDao.getRestaurantById(restaurantId);
        restaurant.removeClassification(classificationId);
        restaurantDao.updateRestaurant(restaurant);
    }
}
