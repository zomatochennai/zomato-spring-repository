package com.ideas2it.zomato.restaurant.dao;

import java.util.Set;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.classification.Classification;

/**
 * Interface implementation for Restaurant dao class.
 */
public interface RestaurantDao {

    /**
     * Inserts a new restaurant into the database.
     *
     * @param restaurant
     *            - restaurant object about to be inserted.
     *
     * @return true or false based on restaurant registration.
     */
    void insertRestaurant(Restaurant restaurant) throws ZomatoException;
    
    /**
     * Updates the restaurant to the database.
     *
     * @param restaurant
     *            - restaurant object to be updated.
     *
     * @throws ZomatoException when updating restaurant fails.
     */
    void updateRestaurant(Restaurant restaurant) throws ZomatoException;
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return restaurant specified by the id.
     */
    Restaurant getRestaurantById(int id) throws ZomatoException;
    
    /**
     * Deletes the restaurant from the database.
     *
     * @param id
     *            - id of restaurant to be deleted.
     */
    void deleteRestaurant(int id) throws ZomatoException;
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return list of restaurants under specified classification.
     */
    Set getRestaurants(int classificationId) throws ZomatoException;
    
    /**
     * Returns the restaurants in the database with matching name specified.
     *
     * @param classificationId
     *            - classification Id of the restaurants.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fails.
     */
    public Set getRestaurantsByName(String restaurantName) 
                                                throws ZomatoException;
}
