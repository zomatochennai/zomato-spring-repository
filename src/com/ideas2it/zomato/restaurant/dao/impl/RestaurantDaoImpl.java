package com.ideas2it.zomato.restaurant.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.ideas2it.zomato.exception.ZomatoException;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.classification.Classification;
import com.ideas2it.zomato.restaurant.dao.RestaurantDao;

/**
 * Performs all the database related funcionalities for food.
 */
@Repository ("restaurantDao")
public class RestaurantDaoImpl implements RestaurantDao {

    private SessionFactory sessionFactory;
    
    /**
     * Inserts a new restaurant into the database.
     *
     * @param restaurant
     *            - restaurant object about to be inserted.
     *
     * @return true or false based on restaurant registration.
     *
     * @throws ZomatoException when inserting restaurant fails.
     */
    public void insertRestaurant(Restaurant restaurant) 
                                    throws ZomatoException {
        sessionFactory.getCurrentSession().save(restaurant);
    }
    
    /**
     * Updates the restaurant to the database.
     *
     * @param restaurant
     *            - restaurant object to be updated.
     *
     * @throws ZomatoException when updating restaurant fails.
     */
    public void updateRestaurant(Restaurant restaurant) throws ZomatoException {
        sessionFactory.getCurrentSession().update(restaurant);
    }
    
    /**
     * Gets the restaurant from database based on the id.
     *
     * @param id
     *            - id of the restaurant.
     *
     * @return restaurant specified by the id.
     *
     * @throws ZomatoException when getting restaurant by id fails.
     */
    public Restaurant getRestaurantById(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Restaurant.class);
        criteria.add(Restrictions.eq("id", id));
        return (Restaurant) criteria.uniqueResult();
    }
    
    /**
     * Deletes the restaurant from the database.
     *
     * @param id
     *            - id of restaurant to be deleted.
     *
     * @throws ZomatoException when deleting restaurant fails.
     */
    public void deleteRestaurant(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Restaurant restaurant = (Restaurant) session.get(Restaurant.class, id);
        session.delete(restaurant);
    }
    
    /**
     * Returns all the restaurants in the database.
     *
     * @param classificationId
     *            - classification Id of the restaurants.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fails.
     */
    public Set getRestaurants(int classificationId) 
                           throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createCriteria(Restaurant.class)
                                  .createCriteria("classifications")
                                  .add( Restrictions
                                  .eq("id", classificationId)).list());
    }
    
    /**
     * Returns the restaurants in the database with matching name specified.
     *
     * @param classificationId
     *            - classification Id of the restaurants.
     *
     * @return set of restaurants.
     *
     * @throws ZomatoException when getting restaurants fails.
     */
    public Set getRestaurantsByName(String restaurantName) 
                                                throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createCriteria(Restaurant.class)
                                  .add( Restrictions
                                  .like("name", restaurantName)).list());
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
