package com.ideas2it.zomato.menuItem;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.category.Category;

/**
 * MenuItem available from a Menu for a particular restaurant. 
 */
public class MenuItem {

    private int id;
    private String name;
    private String description;
    private float price;
    private Restaurant restaurant;
    private Set<Food> foods = new HashSet<>();
    private Set<Category> categories = new HashSet<>();
    
    public MenuItem() {
    
    }
    
    public MenuItem(String name, String description, float price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }  
    
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public float getPrice() {
        return price;
    }
    
    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }
    
    public void setFoods(Set<Food> foods) {
        this.foods = foods;
    }
    
    public Set<Food> getFoods() {
        return foods;
    }
    
    public void addFood(Food food) {
        foods.add(food);
    }
    
    public void setCategories(Set<Category> categories) {
        this.categories = categories;
    }
    
    public Set<Category> getCategories() {
        return categories;
    }
    
    public void addCategory(Category category) {
        categories.add(category);
    }
}
