package com.ideas2it.zomato.menuItem.service;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for MenuItem service class.
 */
public interface MenuItemService {

    /**
     * Registers a new menuItem to the restaurant specified by id.
     *
     * @param restaurantId
     *            - id of restaurant to which menu items need to be added.
     *
     * @param menuItem
     *        - menuItem to be added
     *
     * @param foodIds
     *            - ids of food that belongs to the menuItem.
     *
     * @param categoryIds
     *        - ids of category that belongs to the menuItem.
     *
     * @throws ZomatoException when adding menuItem fails.
     */
    void addMenuItem(int restaurantId, MenuItem menuItem, String[] foodIds,
                     String[] categoryIds) throws ZomatoException;
    
    /**
     * Updates a existing menuItem.
     *
     * @param menuItem
     *            - menuItem object about to be updated .
     *
     * @throws ZomatoException when updating menuItem fails.
     */
    void updateMenuItem(MenuItem menuItem) throws ZomatoException;
    
    /**
     * Gets the menu items for the specified restaurant and category.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return allmenuitems belonging to the restaurant.
     *
     * @throws ZomatoException when getting menuItems fail.
     */
    Set getMenuItems(int restaurantId, int categoryId) 
                               throws ZomatoException;
    
    /**
     * Returns menu item specified by id.
     *
     * @param id
     *            - id of menuItem to be returned.
     *
     * @return menuItem specified by id.
     *
     * @throws ZomatoException when getting menuItem fails.
     */
    MenuItem getMenuItem(int id) throws ZomatoException;
}
