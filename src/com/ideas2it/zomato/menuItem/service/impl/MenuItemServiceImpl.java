package com.ideas2it.zomato.menuItem.service.impl;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.food.Food;
import com.ideas2it.zomato.food.service.FoodService;
import com.ideas2it.zomato.category.Category;
import com.ideas2it.zomato.category.service.CategoryService;
import com.ideas2it.zomato.restaurant.Restaurant;
import com.ideas2it.zomato.restaurant.service.RestaurantService;
import com.ideas2it.zomato.restaurant.service.impl.RestaurantServiceImpl;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.dao.MenuItemDao;
import com.ideas2it.zomato.menuItem.dao.impl.MenuItemDaoImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Service class implementation for menuItem .
 */
@Service("menuItemService")
@Transactional
public class MenuItemServiceImpl implements MenuItemService {

    @Autowired
    private MenuItemDao menuItemDao;
    @Autowired
    private RestaurantService restaurantService;
    @Autowired
    private FoodService foodService;
    @Autowired
    private CategoryService categoryService;

    /**
     * ADD a new menuItem to the restaurant.
     *
     * @param restaurantId
     *        - id of restaurant where menuItem is added.
     *
     * @param menuItem
     *        - menuItem to be added.
     *
     * @param foodIds
     *        - ids of food belonging to the menuItem.
     *
     * @param categoryIds
     *        - ids of category for the menuItem.
     *
     * @throws ZomatoException when adding menuItem fails.
     */
    public void addMenuItem(int restaurantId, MenuItem menuItem, 
                            String[] foodIds, String[] categoryIds) 
                            throws ZomatoException {
        Restaurant restaurant = restaurantService
                                .getRestaurantById(restaurantId);
        menuItem.setRestaurant(restaurant);
        for (int i = 0; i < foodIds.length; i++ ) {
            Food food = foodService.getFoodById(Integer.parseInt(foodIds[i]));
            menuItem.addFood(food);
        }
        for (int i = 0; i < categoryIds.length; i++ ) {
            Category category = categoryService.getCategoryById(Integer
                                               .parseInt(categoryIds[i]));
            menuItem.addCategory(category);
        }
        menuItemDao.insert(menuItem);
    }
    
    /**
     * Get the menu items for the specified restaurant and category.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required.
     *
     * @param categoryId
     *            - category Id whose menu items are required.
     *
     * @return all menuitems belonging to the restaurant in the specified 
     *         category.
     *
     * @throws ZomatoException when getting menuItems fail.
     */
    public Set getMenuItems(int restaurantId, int categoryId) 
                                      throws ZomatoException {
        return menuItemDao.getMenuItems(restaurantId, categoryId);
    }
    
    /**
     * Updates a existing menuItem.
     *
     * @param menuItem
     *            - menuItem to be updated.
     *
     * @throws ZomatoException when updating menuItem fails.
     */
    public void updateMenuItem(MenuItem menuItem) throws ZomatoException {
        menuItemDao.update(menuItem);
    }
    
    /**
     * Returns menuItem specified by id.
     *
     * @param id
     *            - id of menuItem to be returned.
     *
     * @return menu item specified by id.
     *
     * @throws ZomatoException when getting menuItem fails.
     */
    public MenuItem getMenuItem(int id) throws ZomatoException {
        return menuItemDao.getMenuItem(id);
    }
}
