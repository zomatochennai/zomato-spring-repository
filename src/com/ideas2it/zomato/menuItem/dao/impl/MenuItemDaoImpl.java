package com.ideas2it.zomato.menuItem.dao.impl;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.menuItem.dao.MenuItemDao;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Performs all the database related funcionalities for menuitem.
 */
@Repository("menuItemDao")
public class MenuItemDaoImpl implements MenuItemDao {

    private SessionFactory sessionFactory;

    /**
     * Inserts a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem object about to be inserted.
     *
     * @throws ZomatoException when inserting menuItem fails.
     */
    public void insert(MenuItem menuItem) throws ZomatoException {
        sessionFactory.getCurrentSession().save(menuItem);
    }
    
    /**
     * Updates a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem about to be updated.
     *
     * @throws ZomatoException when updating menuItem fails.
     */
    public void update(MenuItem menuItem) throws ZomatoException {
        sessionFactory.getCurrentSession().update(menuItem); 
    }
    
    /**
     * Returns the menuItem for the specified user.
     *
     * @param id
     *            - id of the menu item.
     *
     * @return menu item for the id.
     *
     * @throws ZomatoException when getting menuItem by id fails.
     */
    public MenuItem getMenuItem(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(MenuItem.class);
        criteria.add(Restrictions.eq("id", id));
        return (MenuItem) criteria.uniqueResult();
    }
    
    /**
     * Deletes a menuItem from the database.
     *
     * @param id
     *            - id of menuitem to be deleted
     *
     * @throws ZomatoException when deleting menuItem fails.
     */
    public void delete(int id) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        MenuItem menuItem = (MenuItem) session.get(MenuItem.class, id);
        session.delete(menuItem);
    }
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return allmenuitems belonging to the restaurant.
     *
     * @throws ZomatoException when getting menuItem fails.
     */
    public Set getMenuItems(int restaurantId, int categoryId)
                                      throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createCriteria(MenuItem.class)
                            .createAlias("restaurant", "rest")
                            .createAlias("categories", "cat")
                            .add( Restrictions.eq("rest.id", restaurantId))
                            .add( Restrictions.eq("cat.id", categoryId))
                            .list());
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
