package com.ideas2it.zomato.menuItem.dao;

import java.util.Set;

import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.exception.ZomatoException;

/**
 * Interface implementation for Menuitem dao class.
 */
public interface MenuItemDao {

    /**
     * Inserts a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem object about to be inserted.
     *
     * @throws ZomatoException when inserting menuItem fails.
     */
    void insert(MenuItem menuItem) throws ZomatoException;
    
    /**
     * Updates a new menuItem into the database.
     *
     * @param menuItem
     *            - menuItem about to be updated.
     *
     * @throws ZomatoException when updating menuItem fails.
     */
    void update(MenuItem menuItem) throws ZomatoException;
    
    /**
     * Returns the menuItem for the specified user.
     *
     * @param id
     *            - id of the menu item.
     *
     * @return menuItem for the id.
     *
     * @throws ZomatoException when getting menuItem by id fails.
     */
    MenuItem getMenuItem(int id) throws ZomatoException;
    
    /**
     * Deletes a menuItem from the database.
     *
     * @param id
     *            - id of menuitem to be deleted
     *
     * @throws ZomatoException when deleting menuItem fails.
     */
    void delete(int id) throws ZomatoException;
    
    /**
     * Get the menu items for the specified restaurant.
     *
     * @param restaurantId
     *            - restaurant Id whose menu items are required .
     *
     * @param categoryId
     *            - category Id whose menu items are required .
     *
     * @return allmenuitems belonging to the restaurant.
     *
     * @throws ZomatoException when getting menuItem fails.
     */
    Set getMenuItems(int restaurantId, int categoryId) 
                               throws ZomatoException;
}
