package com.ideas2it.zomato.cart.service;

import java.util.Set;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * Interface implementation for cart service class.
 */
public interface CartService {

    /**
     * Adds a new menuItem to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item.
     *
     * @param quantity
     *            - quantity of menuItem.
     *
     * @param userId
     *            - id of cart's user.
     *
     * @throws ZomatoException if adding menuItem to cart fails.
     */
    public int addMenuItemToCart(int menuItemId, int quantity, int userId) 
                                 throws ZomatoException;
    
    /**
     * Returns the cart for the specified cart id.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @return cart for the specified id.
     *
     * @throws ZomatoException when getting cart by id fails.
     */
    Cart getCartById(int cartId) throws ZomatoException;
}
