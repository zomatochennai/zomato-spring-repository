package com.ideas2it.zomato.cart.service.impl;

import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.cart.dao.CartDao;
import com.ideas2it.zomato.cart.dao.impl.CartDaoImpl;
import com.ideas2it.zomato.cart.service.CartService;
import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.user.service.UserService;
import com.ideas2it.zomato.user.service.impl.UserServiceImpl;
import com.ideas2it.zomato.menuItem.MenuItem;
import com.ideas2it.zomato.menuItem.service.MenuItemService;
import com.ideas2it.zomato.menuItem.service.impl.MenuItemServiceImpl;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This class handles creation and updation of shopping cart.
 */
@Service ("cartService")
@Transactional
public class CartServiceImpl implements CartService {

    private static final Logger logger = Logger.getLogger(CartServiceImpl.class);

    @Autowired
    private CartDao cartDao;
    @Autowired
    private MenuItemService menuItemService;
    @Autowired
    private UserService userService;

    /**
     * Adds a new menuItem to the cart.
     *
     * @param menuItemId
     *            - id of the chosen menu item.
     *
     * @param quantity
     *            - quantity of menuItem.
     *
     * @param userId
     *            - id of cart's user.
     *
     * @return id of user's active cart.
     *
     * @throws ZomatoException if adding menuItem to cart fails.
     */
    public int addMenuItemToCart(int menuItemId, int quantity, int userId) 
                                 throws ZomatoException {
        int cartId = 0;
        Cart cart = getUserCart(userId);
        cart = updateCart(menuItemId, cart, quantity);
        if (0 == cart.getId()) {
            cartId = cartDao.insert(cart);
        } else {
            cartId = cart.getId();
            cartDao.update(cart);
        }
        return cartId;
    }
    
    /**
     * Returns the user's active cart if exists or creates a new cart for 
     * the user.
     *
     * @param userId
     *            - user id of required cart.
     *
     * @return cart for the specified user id.
     *
     * @throws ZomatoException if getting user cart fails.
     */
    private Cart getUserCart(int userId)  throws ZomatoException {
        Cart cart;
        User user = userService.getUserById(userId);
        if (0 != user.getActiveCart().getId()) {
            cart = user.getActiveCart();
        } else {
            cart = new Cart(user);
            cart.setActive(true);
            user.addCart(cart);
        }
        return cart;
    }
    
    /**
     * Adds a menuItem to the cart, if the menuItem already exists the 
     * quantity gets updated.
     *
     * @param menuItemId
     *            - id of menuItem to be updated. 
     *
     * @param cart
     *            - cart that requires update.
     *
     * @param quantity
     *            - quantity of the specified menuItems.
     *
     * @return cart after doing the required updates.
     *
     * @throws ZomatoException if updating cart fails.
     */
    private Cart updateCart(int menuItemId, Cart cart, int quantity)
                                    throws ZomatoException {
        MenuItem menuItem = menuItemService.getMenuItem(menuItemId);
        boolean updateComplete = false;
        for (CartDetail cartDetail : cart.getCartDetails()) {
            if (cartDetail.getMenuItemName().equals(menuItem.getName())) {
                cartDetail.setQuantity(cartDetail.getQuantity() + quantity);
                updateComplete = true;
                break;
            }
        }
        if (!updateComplete) {
            CartDetail cartDetail = new CartDetail(menuItem.getName(), quantity,
                                                   menuItem.getPrice());
            cart.addCartDetail(cartDetail);
            cartDetail.setCart(cart);
        }
        cart.calculateCost();
        return cart;
    }

    /**
     * Returns the cart specified by id.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @return cart for the specified id.
     *
     * @throws ZomatoException when getting cart by id fails.
     */
    public Cart getCartById(int cartId) throws ZomatoException {
        return cartDao.getCartById(cartId);
    }
}
