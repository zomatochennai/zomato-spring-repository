package com.ideas2it.zomato.cart.dao;

import java.util.Set;

import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * This interface handles access between Cart Service and hibernate .
 */
public interface CartDao {
 
    /**
     * Returns the cartDetails for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return cartDetails for the user id.
     *
     * @throws ZomatoException when getting cartDetails by userId fails.
     */
    public Set getCartDetails(int userId) throws ZomatoException;
    
    /**
     * Inserts the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     *
     * @return id of the inserted cart.
     *
     * @throws ZomatoException when inserting cart fails.
     */
    int insert(Cart cart) throws ZomatoException;
    
    /**
     * Updates the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     *
     * @throws ZomatoException when updating cart fails.
     */
    void update(Cart cart) throws ZomatoException;
    
    /**
     * Returns the cart for the specified user.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @return cart for the specified user id.
     */
    Cart getCartById(int cartId) throws ZomatoException;
}
