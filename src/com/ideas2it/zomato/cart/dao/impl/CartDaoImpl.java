package com.ideas2it.zomato.cart.dao.impl;

import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ideas2it.zomato.cart.dao.CartDao;
import com.ideas2it.zomato.cart.Cart;
import com.ideas2it.zomato.cartDetail.CartDetail;
import com.ideas2it.zomato.exception.ZomatoException;

/*
 * CartDaoImpl performs insertion and updation operation of cart object.
 * 
 */
@Repository ("cartDao")
public class CartDaoImpl implements CartDao {

    private SessionFactory sessionFactory;
    private static final Logger logger = Logger.getLogger(CartDaoImpl.class); 
    
    /**
     * Returns the cartDetails for the specified user.
     *
     * @param userId
     *            - id of the user.
     *
     * @return cartDetails for the user id.
     *
     * @throws ZomatoException when getting cartDetails by userId fails.
     */
    public Set getCartDetails(int userId) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return new HashSet(session.createCriteria(CartDetail.class)
                             .createCriteria("cart").createCriteria("user")
                             .add( Restrictions.eq("id", userId)).list());
    }
    
    /**
     * Updates the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     *
     * @throws ZomatoException when updating cart fails.
     */
    public void update(Cart cart) throws ZomatoException {
        sessionFactory.getCurrentSession().update(cart); 
    }
    
    /**
     * Inserts the cart into the database.
     *
     * @param cart
     *            - cart to be updated.
     *
     * @return id of the inserted cart.
     *
     * @throws ZomatoException when inserting cart fails.
     */
    public int insert(Cart cart) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Integer)session.save(cart); 
    }
    
    /**
     * Returns the cart for the specified user.
     *
     * @param cartId
     *            - id of the cart.
     *
     * @return cart for the specified user id.
     */
    public Cart getCartById(int cartId) throws ZomatoException {
        Session session = sessionFactory.getCurrentSession();
        return (Cart)session.createCriteria(Cart.class).add( Restrictions
                            .eq("id", cartId)).uniqueResult();
    }
    
    /**
     * Sets the sessionFactory object .
     *
     * @param sessionFactory
     *            - sessionFactory to be injected.
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
