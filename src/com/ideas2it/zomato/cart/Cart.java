package com.ideas2it.zomato.cart;

import java.util.Set;
import java.util.HashSet;

import com.ideas2it.zomato.user.User;
import com.ideas2it.zomato.order.Order;
import com.ideas2it.zomato.cartDetail.CartDetail;

/**
 * This is model class for Cart. User can add items to Cart till checkout
 * post which an order is created using details from cart 
 */
public class Cart {

    private int id;
    private User user;
    private Set<CartDetail> cartDetails = new HashSet<>();
    private boolean active;
    private float cost;
    private Order order;
    
    public Cart() {
    
    }
    
    public Cart(User user) {
        this.user = user;
    }
    
    /*
     * Setters and Getters.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    } 
    
    public void setUser(User user) {
        this.user = user;
    }
    
    public User getUser() {
        return user;
    }
    
    public void setCartDetails(Set<CartDetail> cartDetails) {
        this.cartDetails = cartDetails;
    }
    
    public Set<CartDetail> getCartDetails() {
        return cartDetails;
    } 
    
    public void setActive(boolean active) {
        this.active = active;
    }
    
    public boolean isActive() {
        return active;
    } 
    
    public void setCost(float cost) {
        this.cost = cost;
    }
    
    public float getCost() {
        return cost;
    } 
    
    public void setOrder(Order order) {
        this.order = order;
    }
    
    public Order getOrder() {
        return order;
    }
    
    public void addCartDetail(CartDetail cartDetail) {
        cartDetails.add(cartDetail);
        calculateCost();
    }
    
    /*
     * Calculates the total cost of the cart by summing the individual 
     * cartDetail totalPrice.
     */
    public void calculateCost() {
        float totalCost = 0.0f;
        for (CartDetail cartDetail : cartDetails) {
            totalCost += cartDetail.getTotalPrice();
        }
        this.cost = totalCost;
    }
}
