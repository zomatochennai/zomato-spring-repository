<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
  <%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
    <link href="/view/media/Food_favicon.ico" rel="shortcut icon">
    <script type = "text/javascript">          
      // Below Function Executes On Form Submit
      function ValidationEvent() {
        // Storing Field Values In Variables
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        // Regular Expression For Email
        var emailReg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        // Conditions
        if (email != '' && password != '') {
          if (email.match(emailReg)) {
            if (password.length >= 8 && password.length <= 15) {
              return true;
            } 
            else {
              alert("The password must be between 8 and 15 characters long!");
              return false;
            }
          } 
          else {
            alert("Please enter a valid email address!");
            return false;
          }
        } 
        else {
          alert("All fields are required!");
          return false;
        }
      }
      
      function displayMessage() {
      var message = document.getElementById("mess").value;
        if (message != '') {
            alert(message);
            return false;
        }
        else {
          return true;
        }
      }
  
    function check(input) {  
    if(input.validity.patternMismatch){  
        input.setCustomValidity("Please enter a valid input");  
    }  
    else {  
        input.setCustomValidity("");  
    }                 
    }
    </script>
  </head>
  <style>
      body {
  background-image: url("view/media/food-and-love.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    img {
    opacity: 0.75;
    filter: alpha(opacity=75);
    }
    .logintable {
      background: rgba(204,204,201,0.6);
    }
    img {
      opacity: 0.75;
      filter: alpha(opacity=75); 
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body bgcolor = "#DEB887" onLoad="return displayMessage()">
    <br><br><br>
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="100" width="200">
        </a>
      </td>
    <tr>
    </table>

    <br><br><br>
    <form method="post" action="login" 
          onsubmit="return ValidationEvent()">
    <table class="logintable" border="1" width="30%" cellpadding="3" align="center">
     <thead>
       <tr>
         <th colspan="2">
           <marquee behavior=alternate>
           <h2>
           Welcome! Please Login Here!
           <h2>
           </marquee>
         </th>
       </tr>
     </thead>
     <tbody>
       <tr>
         <td><strong>Email ID</strong></td>
         <td>
           <input id="email" type="email" name="email" maxlength="30" 
                  placeholder="Enter a valid email address" required
                  pattern=".*\S+.*"  oninput="check(this)"/>
         </td>
       </tr>
       <tr>
         <td><strong>Password</strong></td>
         <td><input id="password" type="password" name="password" maxlength="30"
                    placeholder="Enter Password" required
                    pattern=".*\S+.*"  oninput="check(this)"/>
         </td>
       </tr>
       <tr>
         <td><input type="reset" value="Reset" style="height:30px; width:150px"/></td>
         <td><input type="submit" value="Login" style="height:30px; width:150px"/></td>
       </tr>
       <tr>
         <td><strong><i>Forgot Password?</i></strong></td>
         <td><a href="redirectToPage?targetPage=ForgotPassword">
         <input id="forgotpwd" type="button" name="forgot" value="Reset Password" style="height:30px; width:120px"/> </a></td>
       </tr>
     </tbody>
   </table>
   <input id="mess" type="hidden" value="${message}" name="message" />
     <br>
     </form>
     <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
