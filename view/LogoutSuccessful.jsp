<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    	pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration Successful</title>
    </head>
    <style>
    div.ex {
    	text-align: right width:300px;
    	padding: 10px;
    	border: 5px solid grey;
    	margin: 0px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
    </style>
    <body>
    	<h1>Registration Successful</h1>
    	<div class="ex">
    			<table style="with: 50%">
    				<tr>
    					<td> Registration is Successful. Please Login Here 
    					<br> 
    					<a href='LoginUser.jsp'> Go to Login </a>
    					</td>
    				</tr>
    			</table>
    	</div>
    	<div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
    </body>
    </html>
