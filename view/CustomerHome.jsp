<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ include file="scripts/CustomerRoleCheck.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Customer Home Page</title>
    <script type = "text/javascript">
      function required() {
        var empt = document.forms["search"]["searchRestaurants"].value;
        if (empt == "") {
          alert("Search field cannot be left blank!");
          return false;
        }
        else {
          return true; 
        }
      }
    </script>
  </head>
  <style>
     body {
  background-image: url("view/media/food-and-love.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    img {
      opacity: 0.55;
      filter: alpha(opacity=55); 
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body>
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="redirectToPage?targetPage=CustomerHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                <a href = "getClassifications">
                <input type="button" value="Order Food Online" class="hover" style="height:30px;width:150px"/>
                </a>
                </td>
              </th>
              <th>
                <td>
              <a href = "redirectToPage?targetPage=NewAddressPage">
                <input type="button" value="Add New Address" class="hover" style="height:30px; width:150px"/>
                </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <center><h1>Customer Home Page</h1></center>
    </div>
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=CustomerHome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="115" width="220">
        </a>
      </td>
    <tr>
    </table>
    <br><br>
    <!--
    <div id="top" style="background-color:rgba(204, 204, 204, 0.5);">
      <form:form name="search" action="search" method="post" 
                 onsubmit="required()">
        <center>
        <b><i class="tab blink" >Confused? </i></b> 
        Search your favourite Restaurant : 
        <input type="text" name="searchRestaurants" 
                               placeholder="Search Here!" >
        <input type ="button" value="Search" >
        </center>
      </form:form>
    </div>
     -->
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
    <form method="post" action="updateUser">
        <table style="with: 60%" align="center">
          <tr>
          </tr>
          <tr>
            <td>First Name </td>
            <td>
                <input id="first" type="text" name="firstName" value="${onlineUser.firstName}" maxlength="30"
                       readonly required pattern=".*\S+.*" autocomplete="false" oninput="check(this)" />
            </td>
          </tr>
          <tr>
            <td>Last Name </td>
            <td>
                <input id="lastName" type="text" name="lastName" value="${onlineUser.lastName}" maxlength="30"
                       readonly required/>
            </td>
          </tr>
          <tr>
            <td>Mobile No. </td>
            <td>
                <input id="mobile" type="text" name="mobile" value="${onlineUser.mobile}" maxlength="10"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                       readonly="readonly" required/>
            </td>
          </tr>
          <tr>
            <td>Email </td>
           <td>
               <input id="email" type="email" name="email" value="${onlineUser.email}" maxlength="30"
                      readonly="readonly" required/>
           </td>
           </tr>
         </table>
         <br>
         <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td><a href="redirectToPage?targetPage=CustomerHome">
            <input id="discard" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </a>
            </td>
            <td>
            <input id="save" type="submit" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none'; document.getElementById('first').required = true; document.getElementById('first').removeAttribute('readonly');" />
         </center>
         </form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
