<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css">
    <title>Update Menu Item Page</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
  <script>
$(function(){
    $(".makeEditable").click(function(){
        $(':input').removeAttr("readonly");    
    });
    $(".makeNonEditable").click(function(){
        $(':input').attr("readonly", "readonly");    
    });               
});

//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){  
  /* Select2 plugin as tagpicker */
  $("#tagPicker").select2({
    closeOnSelect:false
  });

}); //script         
      

$(document).ready(function() {});

//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){  
  /* Select2 plugin as tagpicker */
  $("#secondTagPicker").select2({
    closeOnSelect:false
  });

}); //script         
      

$(document).ready(function() {});


  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }
      

$(document).ready(function() {});


  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }
  </script>
  </head>
  <style>
   body {
  background-image: url("view/media/food-and-love.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exleft {
      text-align: left;
      float: left;
      display:inline-block;
      width:45%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exright {
      text-align: right;
      float: right;
      display:inline-block;
      width:45%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body onLoad="return displayMessage()">
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="redirectToPage?targetPage=RestaurantHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="manageRestaurant"> 
                    <input type="button" value="Manage My Restaurant" style="height:30px; width:170px" />
                  </a>
                </td>
              </th>
              <th>
                <td>
                  <a href=""> 
                    <input type="button" value="Manage My Menu" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Manage Restaurant Page</center></h1>
    </div>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
      <form:form method="post" action="updateMenuItem" modelAttribute="menuItem"
            onsubmit="return ValidationEvent()">
        <table style="with: 60%" align="center">
            <caption style="font-size:20px"><b>Basic Menu Item Details:</b></caption>
          <tr><link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
            <td>Menu Item Name *</td>
            <td><form:form path="name"/>
                <input id="name" type="text" name="name" value="${menuItem.name}"
                      style="height:15px; width:170px" readonly="readonly" required/>
            </td>
          </tr>
          <tr>
            <td>Menu Item Description *</td>
            <td><form:form path="description" />
                <input id="description" type="text" name="description"  value="${restaurant.description}"
                style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Menu  Item Price (in &#8377; only)  *  </td>
            <td>
            <form:form path="price"/>
            <input type="number" step="0.01" value="" placeholder="00.00" name="quantity" min="0" 
                   style="height:25px; width:200px" readonly="readonly" required />
            </td>
          </tr>
         </table>
         <br>
         <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td>
            <input id="discard" class="makeNonEditable" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(false);" />
            </td>
            <td>
            <input id="save" class="makeNonEditable" type="button" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementBy  float: left;Id('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(true); return confirm('Are you sure? Press OK to continue')" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" class="makeEditable" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none';" />
         </center>
         </form:form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
      <br><br>
    <div id="manage" class="exleft" align="left" style="background-color:rgba(204, 204, 204, 0.8); margin: 1%">
        <table style="with: 5%" align="left">
            <tr>
                <td>
                Update food Type for your menu Item!
                </td>
            </tr>
          <c:forEach var="food" items="${menuitem.foods}">
            <tr>
              <td>
                  <option value="${food.id}" selected>
                    <c:out value="${food.name}" />
                  </option>
        </select>
                </td>
                <td>
               <input id="update" class="makeEditable" type="button" value="Delete" style="height:25px; width:80px;"
                      onclick="return confirm('Are you sure you want to Delete?')" />                 
                </td>
              </tr>
          </c:forEach>
       </table>
    </div>
    <div id="manage" class="exright" align="right" style="background-color:rgba(204, 204, 204, 0.8); margin: 1%">
        <table style="with: 5%" align="right">
            <tr>
                <td>
                Add your menu item to more food types!
                </td>
            </tr>
            <tr>
                <td>
           <select class="form-control" id="tagPicker" multiple="multiple" style="height:30px; width:425px" placeholder="Search Here!">
          <c:forEach var="food" items="${foods}">
                  <option value="${food.id}">
                    <c:out value="${food.name}" />
                  </option>
                </c:forEach>
        </select>
                </td>
                <td>
<input id="update" class="makeEditable" type="button" value="Update" style="height:40px; width:135px;" />                 
                </td>
              </tr>
       </table>
    </div>
      <br><br>
    <div id="manage" class="exleft" align="left" style="background-color:rgba(204, 204, 204, 0.8); margin: 1%">
        <table style="with: 5%" align="left">
            <tr>
                <td>
                Update categories for your food item!
                </td>
            </tr>
          <c:forEach var="category" items="${menuitem.categories}">
            <tr>
              <td>
                  <option value="${category.id}" selected>
                    <c:out value="${category.name}" />
                  </option>
        </select>
                </td>
                <td>
               <input id="update" class="makeEditable" type="button" value="Delete" style="height:25px; width:80px;"
                      onclick="return confirm('Are you sure you want to Delete?')" />                 
                </td>
              </tr>
          </c:forEach>
       </table>
    </div>
    <div id="manage" class="exright" align="right" style="background-color:rgba(204, 204, 204, 0.8); margin: 1%">
        <table style="with: 5%" align="right">
            <tr>
                <td>
                Add more categories to your Menu Item? No problem!
                </td>
            </tr>
            <tr>
                <td>
           <select class="form-control" id="secondTagPicker" multiple="multiple" style="height:30px; width:425px" placeholder="Search Here!">
          <c:forEach var="category" items="${categories}">
                  <option value="${category.id}">
                    <c:out value="${category.name}" />
                  </option>
                </c:forEach>
        </select>
                </td>
                <td>
<input id="update" class="makeEditable" type="button" value="Update" style="height:40px; width:135px;" />                 
                </td>
              </tr>
       </table>
    </div>
  </body>
</html>
