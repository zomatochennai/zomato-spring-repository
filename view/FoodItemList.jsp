<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="e" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Menu Page</title>
    <script type = "text/javascript">
    function displayMessage() {
      var message = document.getElementById("mess").value;
        if (message != '') {
            alert(message);
            return false;
        }
        else {
          return true;
        }
      }
      function required() {
        var empt = document.forms["search"]["searchRestaurants"].value;
        if (empt == "") {
          alert("Search field cannot be left blank!");
          return false;
        }
        else {
          return true; 
        }
      }
    </script>
  </head>
  <style>
   body {
  background-image: url("view/media/spices_seasonings.jpg");
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body onLoad="return displayMessage()">
     <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" /></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
         <tr>
           <td>
           <a href = "viewCart">
           <input type="button" value="View Cart" style="height:30px; width:100px"/></td>
           </a>
         </tr>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.5);">
   <h1><center>Restaurant Menu Page</center></h1>
   </div>
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=CustomerHome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="115" width="220">
        </a>
      </td>
    <tr>
    </table>
    <!--
    <div id="top" style="background-color:rgba(204, 204, 204, 0.5);">
      <form:form name="search" action="search" method="post" 
                 onsubmit="required()">
        <center>
        <b><i class="tab blink" >Confused? </i></b> 
        Search your favourite Restaurant : 
        <input type="text" name="searchRestaurants" 
                               placeholder="Search Here!" >
        <input type ="button" value="Search" >
        </center>
      </form:form>
    </div>
     -->
    <br>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.6);">
      <form action="addToCart" method="post">
        <table style="with: 50%" align="center">
          <center><h2>Restaurant Details</h2></center>
           <tr>
             <th>${restaurant.name}</th>
             <th>${restaurant.description}</th>
             <th>${restaurant.startingTime}</th>
             <th>${restaurant.closingTime}</th>
           </tr>
         </table>
         <br>
         <table style="with: 20%" align="center">
           <tr>
             <th>${catogory.name}</th>
           </tr>
         </table>
         <table style="with: 80%" align="center">
           <h3>
           <center>
             List of ${catogery.name} Options Available 
               Options at ${restaurant.name}
           </center>
           </h3>
           <tr>
             <th>| Menu Item Name |</th>
             <th> Description  |</th>
             <th> Price Per Unit |</th>
             <th> Quantity Ordered |</th>
             <th> Order Food |</th>
           </tr>
           <tr>
             <e:forEach var="menuItem" items="${menuItems}">
               <td>| <e:out value="${menuItem.name}" /> |</td>
               <td> <e:out value="${menuItem.description}" /> |</td>
               <td> <e:out value="${menuItem.price}" /> |</td>
               <td> <input type="number" value="1" name="quantity" min="1" max="500" />
               </td>
               <td>
               <input type="submit" value="Add to Cart" style="height:30px; width:120px"/>
               <input type="hidden" value="${categoryId}" name="categoryId" />
               <input type="hidden" value="${menuItem.id}" name="id" />
               </td>
             </e:forEach>
           </tr>
         </table>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </form>
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
