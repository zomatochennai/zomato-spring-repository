<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add New Address Page</title>
    <script type = "text/javascript">
      function required() {
        var empt = document.forms["search"]["searchRestaurants"].value;
        if (empt == "") {
          alert("Search field cannot be left blank!");
          return false;
        }
        else {
          return true; 
        }
      }
    </script>
  </head>
  <style type "text/css">
       body {
  background-image: url("view/media/all-food-basic-hd.jpg");
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    img {
      opacity: 0.65;
      filter: alpha(opacity=65); 
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body>
     <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" /></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="left">
         <tr>
           <td>
           <a href = "viewCart">
           <input type="button" value="View Cart" style="height:30px; width:100px"/></td>
           </a>
         </tr>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center>New Address Addition Form</center></h1>
    </div>
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=CustomerHome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="115" width="220">
        </a>
      </td>
    <tr>
    </table>
    
     <!--
    <div id="top" style="background-color:rgba(204, 204, 204, 0.5);">
      <form:form name="search" action="search" method="post" 
                 onsubmit="required()">
        <center>
        <b><i class="tab blink" >Confused? </i></b> 
        Search your favourite Restaurant : 
        <input type="text" name="searchRestaurants" 
                               placeholder="Search Here!" >
        <input type ="button" value="Search" >
        </center>
      </form:form>
    </div>
     -->
    <br>
     <div class="ex" style="background-color:rgba(204, 204, 204, 0.3);">
     <form action="addAddress" method="post">
       <table style="with: 60%" align="center">
         <tr>
           <td>Address Line 1 *</td>
           <td><input type="text" name="addressLineOne" maxlength="30"
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
         <tr>
           <td>Address Line 2</td>
           <td><input type="text" name="addressLineTwo" maxlength="30"
                      placeholder="This field is optional" />
           </td>
         </tr>
         <tr>
           <td>City *</td>
           <td><input type="text" name="city" maxlength="30"
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
         <tr>
           <td>State *</td>
           <td><input type="text" name="state" maxlength="30"
                      placeholder="Please Enter a value for this field"  
                      required/>
           </td>
         </tr>
         <tr>         
           <td>Pin Code *</td>
           <td><input type="integer" name="pincode" maxlength="6"
                      onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                      placeholder="Please Enter a value for this field" 
                      required/>
           </td>
         </tr>
       </table>
       <br>
       <center>
       <i>Fields marked with * are required fields</i>
       <br><br>
        <input type="reset" value="Reset" style="height:40px; width:120px"/>
        &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp;
        <input type="submit" value="Add Address" style="height:40px; width:120px"/>
        </center>
      </form>
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
