<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css">
    <title>Manage Restaurant Page</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
$(function(){
    $(".makeEditable").click(function(){
        $(':input').removeAttr("readonly");    
    });
    $(".makeNonEditable").click(function(){
        $(':input').attr("readonly", "readonly");    
    });               
});

$(document).ready(function(){
    $('input.timepicker').timepicker({
    timeFormat: 'hh:mm p',
    interval: 5,
    minTime: '00:00 am',
    maxTime: '23:59 pm',
    defaultTime: '',
    startTime: '00:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
    });
});

//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){  
  /* Select2 plugin as tagpicker */
  $("#tagPicker").select2({
    closeOnSelect:false
  });

}); //script         
      

$(document).ready(function() {});


  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }
  </script>
  </head>
  <style>
   body {
  background-image: url("view/media/food-and-love.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exleft {
      text-align: left;
      float: left;
      display:inline-block;
      width:45%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exright {
      text-align: right;
      float: right;
      display:inline-block;
      width:45%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body onLoad="return displayMessage()">
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="goToHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="manageRestaurant"> 
                    <input type="button" value="Manage My Restaurant" style="height:30px; width:170px" />
                  </a>
                </td>
              </th> <!--
              <th>
                <td>
                  <a href="manageMenu"> 
                    <input type="button" value="Manage My Menu" style="height:30px; width:150px" />
                  </a>
                </td>
              </th> -->
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Manage Restaurant Page</center></h1>
    </div>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
      <form:form method="post" action="updateRestaurant" modelAttribute="restaurant"
            onsubmit="return ValidationEvent()">
        <table style="with: 60%" align="center">
            <caption style="font-size:20px"><b>Basic Restaurant Details:</b></caption>
          <tr><link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
          <input id="name" type="hidden" name="id" value="${restaurant.id}"/>
            <td>Restaurant Name *</td>
            <td><form:form path="name"/>
                <input id="name" type="text" name="name" value="${restaurant.name}"
                      style="height:15px; width:170px" readonly="readonly" required/>
            </td>
          </tr>
          <tr>
            <td>Restaurant Description *</td>
            <td><form:form path="description" />
                <input id="description" type="text" name="description"  value="${restaurant.description}"
                style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Starting Time  *</td>
            <td><form:form path="startingTime"/>
                <input id="startingTime" class="timepicker text-center" value="${restaurant.startingTime}"
                 jt-timepicker="" name="startingTime"
                      time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" 
                      time-format="model.options.timeFormat" start-time="model.options.startTime"
                        min-time="model.options.minTime" 
                      max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" 
                      scrollbar="model.options.scrollbar" dropdown="model.options.dropdown" 
                      readonly="readonly" required />
            </td>
          </tr>
           <tr>
            <td>Closing Time  *</td>
            <td><form:form path="closingTime"/>
            <input id="closingTime" class="timepicker text-center" value="${restaurant.closingTime}"
            jt-timepicker="" name="closingTime"
                      time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" 
                      time-format="model.options.timeFormat" start-time="model.options.startTime" min-time="model.options.minTime" 
                      max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" 
                      scrollbar="model.options.scrollbar" dropdown="model.options.dropdown"
                      readonly="readonly" required />
            </td>
          </tr>
           <tr>
            <td>Home Delivery *</td>
            <td><form:form path="homeDelivery"/>
                <input type="radio" name="homeDelivery" value="true" checked> Yes
                <input type="radio" name="homeDelivery" value="false"> No
            </td>
          </tr>
          
          <tr>
            <td>Restaurant Address Line 1 *</td>
            <td><form:form path="address.addressLineOne" />
                <input id="addressLineOne" type="text" name="addressLineOne"
                 value="${restaurant.address.addressLineOne}" readonly="readonly"
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Restaurant Address Line 2</td>
            <td><form:form path="address.addressLineTwo" />
                <input id="addressLineTwo" type="text" name="addressLineTwo" 
                value="${restaurant.address.addressLineTwo}" readonly="readonly"
                        style="height:15px; width:170px"/>
            </td>
          </tr>
          <tr>
            <td>City *</td>
            <td><form:form path="address.city"/>
                <input id="city" type="text" name="city" 
                value="${restaurant.address.city}" readonly="readonly"
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>State *</td>
            <td><form:form path="address.state" />
                <input id="state" type="text" name="state"
                value="${restaurant.address.state}" readonly="readonly"
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Pin Code *</td>
            <td><form:form path="address.pincode" />
                <input id="pincode" type="text" name="pincode" value="${restaurant.address.pincode}"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )" style="height:15px; width:170px" 
                         readonly="readonly" required/>
            </td>
            
         </table>
         <br>
         <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td>
            <input id="discard" class="makeNonEditable" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(false);" />
            </td>
            <td>
            <input id="save" class="makeNonEditable" type="submit" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementBy  float: left;Id('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(true); return confirm('Are you sure? Press OK to continue')" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" class="makeEditable" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none';" />
         </center>
         </form:form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
      <br><br>
    <div id="manage" class="exleft" align="left" style="background-color:rgba(204, 204, 204, 0.8);">
        <table style="with: 5%" align="left">
            <tr>
                <td>
                Update classifications for your restaurant!
                </td>
            </tr>
          <c:forEach var="classification" items="${restaurant.classifications}">
            <tr>
              <td>
                  <option value="${classification.id}" selected>
                    <c:out value="${classification.name}" />
                  </option>
        </select>
                </td>
                <td>
                <a href="removeRestaurantClassification?classificationId=${classification.id}">
               <input id="delete" class="makeEditable" type="button" value="Delete" 
               style="height:25px; width:80px;"
                      onclick="return confirm('Are you sure you want to Delete?') ;
                        window.location.reload(true);" />     
                 </a>            
                </td>
              </tr>
          </c:forEach>
       </table>
    </div>
    <div id="manage" class="exright" align="right" style="background-color:rgba(204, 204, 204, 0.8);">
      <form action="assignClassifications" method="post">
        <table style="with: 5%" align="right">
            <tr>
                <td>
                Need more classifications to your restaurant? No problem!
                </td>
            </tr>
            <tr>
                <td>
           <select class="form-control" name="classificationIdList" id="tagPicker" multiple="multiple" style="height:30px; width:425px" placeholder="Search Here!">
          <c:forEach var="classification" items="${classifications}">
                  <option value="${classification.id}">
                    <c:out value="${classification.name}" />
                  </option>
                </c:forEach>
        </select>
                </td>
                <td>
               <input id="update" type="submit" value="Update" 
                      style="height:40px; width:135px;" />                 
                </td>
              </tr>
       </table>
       </form>
    </div>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  </body>
</html>
