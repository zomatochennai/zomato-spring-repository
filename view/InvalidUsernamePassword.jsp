<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
  <%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>User Data</title>
  </head>
  <style>
 body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: center width:300px; height: 730px;
      padding: 20px;
      border: 02px solid grey;
      margin: 10px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body bgcolor = "#CBB8FF">
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.2);">
    <center>
    <h1>
      <p class="tab blink" style="color:maroon;">
        <br>
        !!! Invalid Username or Password !!!
        <br>
      </p>
    </h1>
    </center>
    <br><br>
    <br><br>
      <table style="with: 200%"  border="0" width="50%" height="50%" cellpadding="4" align="center">
        <tr>
          <td>
            <center><b><i><marquee behavior=alternate>
              <h2>
              You have entered an invalid Username and/or Password
              <h2>
            </marquee></i></b></center>
          </td>
        </tr>
        <tr>
          <td>
            <center>
            <h3>
            Try again? Please login 
            <a href = "redirectToPage?targetPage=LoginUser">here</a>.
            </h3></center>
          </td>
        </tr>
        <tr>
          <td>
            <center>
            <h3>
            New User? Please register 
            <a href = "redirectToPage?targetPage=RegisterUser">here</a>.
            </h3></center>
          </td>
        </tr>
        <tr>
          <td>
            <center>
            <h3>
            Forgot Password? 
            <a href = "redirectToPage?targetPage=ForgotPassword">Reset Password</a>.
            </h3></center>
          </td>
        </tr>
      </table>
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
