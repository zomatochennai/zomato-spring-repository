
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration Page</title>
    <script type = "text/javascript">          
      // Below Function Executes On Form Submit
      function ValidationEvent() {
        // Storing Field Values In Variables
        var mobile = document.getElementById("mobile").value;
        var pincode = document.getElementById("pincode").value;        
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        // Regular Expression For Email
        var emailReg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        // Conditions
          if (email.match(emailReg)) {
            if (password.length >= 8 && password.length <= 15) {
              if (mobile.length == 10) {
                if (pincode.length == 6) {
                  return true;
                }
                else {
                  alert("The PIN Code must be 6 numbers long!");
                  return false;
                }
              }
              else {
                alert("The mobile number must be 10 numbers long!");
                return false;
              } 
            } 
            else {
              alert("The password must be between 8 and 15 characters long!");
              return false;
            }
          } 
          else {
            alert("Enter a valid email address!");
            return false;
          }
      }
      
      function displayMessage() {
      var message = document.getElementById("mess").value;
        if (message != '') {
            alert(message);
            return false;
        }
        else {
          return true;
        }
      }

    
    function check(input) {  
      if(input.validity.patternMismatch){  
        input.setCustomValidity("Please enter a valid input");  
      }  
      else {  
        input.setCustomValidity("");  
      }                 
    } 
    </script>
  <style>
 body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body onLoad="return displayMessage()">
            <table style="with: 5%" align="left">
              <th>
                <td>
                  <a href="redirectToPage?targetPage=Welcome"> 
                    <input type="button" value="Go Back to Welcome Page" style="height:30px; width:180px" />
                  </a>
                </td>
              </th>
       </table>
            <table style="with: 5%" align="right">
              <th>
                <td>
                  <a href="redirectToPage?targetPage=LoginUser"> 
                    <input type="button" value="Go to Login Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.5);">
    <h1><center>Registration Form</center></h1>
    </div>
<table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="100" width="200">
        </a>
      </td>
    <tr>
    </table>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.4);">
      <form:form method="post" action="register" modelAttribute="address"
            onsubmit="return ValidationEvent()">
        <table style="with: 60%" align="center">
          <tr>
            <td>Please Select Type of User: *</td>
            <td>
              <select name="roleId">
                <c:forEach var="role" items="${roles}">
                  <option value="${role.id}">
                    <c:out value="${role.name}" />
                  </option>
                </c:forEach>
              </select>
            </td>
          </tr>
          <tr>
          </tr>
          <tr>
            <td>First Name *</td>${message}
            <td><form:form path="user.firstName"/>
                <input id="firstName" type="text" name="firstName" maxlength="30"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>Last Name *</td>
            <td><form:form path="user.lastName" />
                <input id="lastName" type="text" name="lastName" maxlength="30"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>Mobile No. *</td>
            <td><form:form path="user.mobile"/>
                <input id="mobile" type="text" name="mobile" maxlength="10"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
                       &nbsp;<span id="errmsg"></span>
            </td>
          </tr>
          <tr>
            <td>Address Line 1 *</td>
            <td><form:form path="addressLineOne" />
                <input id="addressLineOne" type="text" name="addressLineOne" maxlength="30"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>Address Line 2</td>
            <td><form:form path="addressLineTwo" />
                <input id="addressLineTwo" type="text" name="addressLineTwo" maxlength="30"
                       placeholder="This field is optional"
                       pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>City *</td>
            <td><form:form path="city"/>
                <input id="city" type="text" name="city" maxlength="30"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>State *</td>
            <td><form:form path="state" />
                <input id="state" type="text" name="state" maxlength="30"
                       placeholder="Enter a value for this field"  
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>Pin Code *</td>
            <td><form:form path="pincode" />
                <input id="pincode" type="text" name="pincode" maxlength="6"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                       placeholder="Enter a value for this field" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
            </td>
          </tr>
          <tr>
            <td>Email *</td>
           <td><form:form path="user.email"/>
               <input id="email" type="email" name="email" maxlength="30"
                      placeholder="Enter a value for this field" 
                      required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
           </td>
           </tr>
           <tr>
             <td>Password *</td>
             <td><form:form path="user.password"  />
                 <input id="password" type="password" name="password" maxlength="30" 
                        placeholder="Enter a value for this field" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
             </td>
           </tr>
           <tr>
             <td>Security Question: Enter your pet name *</td>
             <td><form:form path="user.petName"/>
                 <input id="petName" type="password" name="petName" maxlength="30"
                        placeholder="Enter a value for this field" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
             </td>
           </tr>
           <tr>
             <td>Security Question: Enter your first bicycle name *</td>
             <td><form:form path="user.bicycleName" />
                 <input id="bicycleName" type="password" name="bicycleName" maxlength="30"
                        placeholder="Enter a value for this field" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/>
             </td>
           </tr>
         </table>
         <br>
         <i><center>Fields marked with * are required fields</center></i>
         <br>
         <center>
         <input type="reset" value="Reset" style="height:30px; width:120px" />
         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
         <input type="submit" value="Register" style="height:30px; width:120px" />
         <input id="mess" type="hidden" value="${message}" name="message" />
         </center>
       </form:form>
     </div>
     <br><br><br>
     <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
