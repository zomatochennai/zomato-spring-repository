<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
  <%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Forgot Password</title>
    <script type="text/javascript">
      // Below Function Executes On Form Submit
      function ValidationEvent() {
        // Storing Field Values In Variables
        var mobile = document.getElementById("mobile").value;
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        var pincode = document.getElementById("confirmPassword").value;        
        // Regular Expression For Email
        var emailReg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        // Conditions
          if (email.match(emailReg)) {
            if (password.length >= 8 && password.length <= 15 && confirmPassword.length >= 8 && confirmPassword.length <= 15) {
              if (mobile.length == 10) {
                if (password != confirmPassword) {
                  return true;
                }
                else {
                  alert("Entered password(s) do no match!");
                  return false;
                }
              }
              else {
                alert("The mobile number must be 10 numbers long!");
                return false;
              } 
            } 
            else {
              alert("The password must be between 8 and 15 characters long!");
              return false;
            }
          } 
          else {
            alert("Enter a valid email address!");
            return false;
          }
      }
      
    function displayMessage() {
      var message = document.getElementById("mess").value;
        if (message != '') {
            alert(message);
            return false;
        }
        else {
          return true;
        }
      }
    
    function check(input) {  
      if(input.validity.patternMismatch){  
        input.setCustomValidity("Please enter a valid input");  
      }  
      else {  
        input.setCustomValidity("");  
      }                 
    }
    </script>
  </head>
  <style>
   body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    div.ex {
      text-align: right width:500px; height:250px;
      padding: 20px;
      border: 8px solid grey;
      margin: 2px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body onLoad="return displayMessage()">
       <table style="with: 5%" align="right">
         <tr>
           <td>
           <a href = "redirectToPage?targetPage=LoginUser">
           <input type="button" value="Back to Login" style="height:30px; width:150px"/></td>
           </a>
         </tr>
       </table>
       <table style="with: 5%" align="left">
         <th>
           <td>
            &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;
           </td>
         </th>
         <th>
           <td>
            &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;
           </td>
         </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.5);">
    <h1><center>Reset Password</center></h1>
    </div>
    <br><br>
<table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="100" width="200">
        </a>
      </td>
    <tr>
    </table>
    <br><br>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.6);">
      <form action="resetPassword" method="post" onsubmit="return ValidationEvent()">
        <table style="with: 60%; font-family: Arial, Verdana, sans-serif; font-size:1em;" align="center">
            <td>Registered Email</td>
            <td><input id="email" type="email" name="email" maxlength="30" 
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
          </tr>
          <tr>
            <td>Registered Mobile No.</td>
            <td><input id="mobile" type="text" name="mobile" maxlength="10"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                       required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
           </tr>
           <tr>
             <td>Enter New Password</td>
             <td><input id="password" type="password" name="password" maxlength="30" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
           </tr>
           <tr>
           <tr>
             <td> Confirm New Password</td>
             <td><input id="confirmpassword" type="password" name="comfirmpassword" maxlength="30" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
           </tr>
           <tr>
             <td>Security Question: What is your pet name?</td>
             <td><input id="questionOne" type="password" name="petName" maxlength="30" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
           </tr>
           <tr>
             <td>Security Question: Enter your first bicycle name?
             </td>
             <td><input id="questionTwo" type="password" name="bicycleName" maxlength="30" 
                        required pattern=".*\S+.*" autocomplete="off" oninput="check(this)"/></td>
           </tr>
           <tr>
             <td>
             <center><i><br>
                ***All fields are required fields*** 
             <br></i></center>
             </td>
           </tr>
        </table>
        <center>
        <br>
        <input type="reset" value="Reset" style="height:30px; width:120px"/> 
        &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp;
        <input type="submit" value="Reset Password" style="height:30px; width:150px"/>
        </center>
      </form>
      <input id="mess" type="hidden" value="${message}" name="message" />
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>

