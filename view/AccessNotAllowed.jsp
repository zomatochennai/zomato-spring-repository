<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Access Denied!!!</title>
  </head>
  <style type "text/css">
   body {
  background-image: url("view/media/access-denied-bg.jpg");
  }
    .blink {
      -webkit-animation: blink .75s linear infinite;
      -moz-animation: blink .75s linear infinite;
      -ms-animation: blink .75s linear infinite;
      -o-animation: blink .75s linear infinite;
       animation: blink .75s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px white;
      margin: 5px
    }
  </style>
  <body bgcolor = "#F08080">
    <div style="background-color:rgba(204, 204, 204, 0.8);">
    <center><h1><b><p>Access Not Allowed!!</p></b></h1></center>
    </div>
    <div class="ex">
      <center>
      <br><br>
    <div style="background-color:rgba(204, 204, 204, 0.3);">
      <img border="0" alt="STOP!" src="view/media/access_denied.png" 
           width="400" height="200">
      <img class="tab blink" border="0" alt="STOP!" src="view/media/access-denied-red.png" 
           width="400" height="200">
      <img border="0" alt="STOP!" src="view/media/access-denied.png" 
           width="400" height="200">
    </div>
      <br><br><br>
      <div style="background-color:rgba(204, 204, 204, 0.6);">
      <h2>You are trying to access the part of website you have no access for!</h2>
      <h3>You can go back <a href = "/zomato/goToHome" >here</a> and start again.<h3>
      </div>
      <br><br>
      <img border="0" alt="Not Allowed!" src="view/media/logo-accessdenied.png" 
           width="150" height="150">
      <img border="0" alt="Not Allowed!" src="view/media/StopIT.png" width="150"
           height="150">
      </center>      
    </div>
  </body>
</html>
