      // Below Function Executes On Form Submit
      function ValidationEvent() {
        // Storing Field Values In Variables
        var email = document.getElementById("email").value;
        var password = document.getElementById("password").value;
        // Regular Expression For Email
        var emailReg = /^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!\.)){0,61}[a-zA-Z0-9]?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/;
        // Conditions
        if (email != '' && password != '') {
          if (email.match(emailReg)) {
            if (password.length >= 8 && password.length <= 15) {
              return true;
            } 
            else {
              alert("The password must be between 8 and 15 characters long!");
              return false;
            }
          } 
          else {
            alert("Please enter a valid email address!");
            return false;
          }
        } 
        else {
          alert("All fields are required!");
          return false;
        }
      }
