<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ include file="scripts/CacheManagement.jsf" %> 
<%@ include file="scripts/CookieManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Classification Page</title>
    <script type = "text/javascript">
      function required() {
        var empt = document.forms["search"]["searchRestaurants"].value;
        if (empt == "") {
          alert("Search field cannot be left blank!");
          return false;
        }
        else {
          return true; 
        }
      }
    </script>
  </head>
  <style>
     body {
  background-image: url("view/media/spices_seasonings.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    img {
      opacity: 0.75;
      filter: alpha(opacity=75); 
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body>
     <form:form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px"/></td>
         </tr>
       </table>
    </form:form>
       <table style="with: 5%" align="right">
         <tr>
           <td>
           <a href = "viewCart">
           <input type="button" value="View Cart" style="height:30px; width:100px"/></td>
           </a>
         </tr>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center>Restaurant Classification List</center></h1>
    </div>
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=CustomerHome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="115" width="220">
        </a>
      </td>
    <tr>
    </table>
    <br><br>
    <!--
    <div id="top" style="background-color:rgba(204, 204, 204, 0.5);">
      <form:form name="search" action="search" method="post" 
                 onsubmit="required()">
        <center>
        <b><i class="tab blink" >Confused? </i></b> 
        Search your favourite Restaurant : 
        <input type="text" name="searchRestaurants" 
                               placeholder="Search Here!" >
        <input type ="button" value="Search" >
        </center>
      </form:form>
    </div>
     -->
    <br>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.6);">
        <table style="with: 50%" align="center">
          <center><h2>List of Restaurants Classification</h2></center>

          <tr>
            <th>Restaurant Classification</th>
            <th>View Restaurants</th>            
          </tr>
          <c:forEach var="classification" items="${classifications}">
            <tr>
              <td>
                <c:out value="${classification.name}" />
              </td>
              <td>
                <a href="getRestaurants?id=${classification.id}"> 
                <input  type="button" value="View Restaurants" style="height:30px; width:150px"/>
                </a> 
              </td>
            </tr>
          </c:forEach>
        </table>
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
