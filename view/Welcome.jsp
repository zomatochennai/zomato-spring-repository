<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
  <%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome Page</title>
    <script type = "text/javascript">
    function displayMessage() {
      var message = document.getElementById("mess").value;
        if (message != '') {
            alert(message);
            return false;
        }
        else {
          return true;
        }
      }

$(document).ready(function() {
	
	
});
    </script>
  </head>
  <style type "text/css">
 body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    .welcometable {
      background: rgba(204,204,201,0.6);
    }
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px blue;
      margin: 5px
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body bgcolor="#B8860B" onLoad="return displayMessage()">
    <table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="100" width="200">
        </a>
      </td>
    <tr>
    </table>
<center>
    <div style="background-color:rgba(204, 204, 204, 0.5);">
    <h1>
      <p class="tab blink" style="color:red;">
        Welcome to 'Ideas 2 Eat'!
      </p>
    </h1>
    </div>
    </center>
    <center>
      <h3>
      <div style="background-color:rgba(204, 204, 204, 0.6);">
      <marquee behavior=alternate>
      <p style=";">
        Any idea where you want to eat today?! We got it!
      </p>
      </marquee>
      </div>
    </h3>
    </p>
    <div class="ex">
      <table class="welcometable" style="with: 20%" border="10" BORDERCOLOR="#A52A2A" width="25%" cellpadding="25">
        <tr>      
          <td>
            <center>
            <h3>
            <a href = "redirectToPage?targetPage=LoginUser">
              <input type="button" value="Login" style="height:40px; width:120px" />
            </a>
            <h3>
            </center>
          </td>
        </tr>
        <tr>
          <td>
            <center>
            <h3>
            <a href = "redirectToRegister">
              <input type="button" value="Register" style="height:40px; width:120px" />
            </a>
            </h3>
            </center>
          </td>
        </tr>    
      </table>
      <input id="mess" type="hidden" value="${message}" name="message" />
    </div>
    <br>
     <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
