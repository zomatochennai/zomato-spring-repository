<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="d" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="e" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css">
<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2-bootstrap.css">
    <title>Manage Menu Page</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  </head>
  <style>
   body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    div.exleft {
      text-align: left;
      float: left;
      display:inline-block;
      width:35%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exright {
      text-align: right;
      float: right;
      display:inline-block;
      width:60%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body onLoad="return displayMessage()">
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="redirectToPage?targetPage=RestaurantHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="manageRestaurant"> 
                    <input type="button" value="Manage Restaurant" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
              <th>
                <td><%@ include file="scripts/CookieManagement.jsf" %>
                  <a href="manageMenu"> 
                    <input type="button" value="Manage Menu Items" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Manage Menu Page</center></h1>
    </div>
    <br><br>
    <div class="exleft" style="background-color:rgba(204, 204, 204, 0.7);">
      <form:form method="post" action="addMenuItem" modelAttribute="menuItem"
            onsubmit="return ValidationEvent()">
        <table style="with: 60%" align="center">
            <caption style="font-size:18px"><b>Add menu item to your restaurant!</b></caption>
          <tr>
            <td><br></td>
          </tr>
          <tr>     
            <td style="padding-left:7em">Basic Menu Item Detail:</td>
          </tr>
          <tr>
            <td></td>
          </tr>
          <tr>
            <td>Menu Item Name *</td>
            <td><form:form path="name"/>
                <input id="name" type="text" name="name"
                      style="height:20px; width:200px" required/>
            </td>
          </tr>
          <tr>
            <td>Menu Item Description *</td>
            <td><form:form path="description" />
                <input id="description" type="text" name="description" 
                style="height:20px; width:200px" required/>
            </td>
          </tr>
           <tr>
            <td>Menu  Item Price (in &#8377; only)  *  </td>
            <td>
            <form:form path="price"/>
            <input type="number" step="1.00" value="" placeholder="00.00" name="price" min="0" style="height:25px; width:200px" required />
            </td>
          </tr>
          <tr>
           <tr>
            <td></td>
          </tr>
           <tr>
            <td>Restaurant *</td>
            <td> ${restaurant.name} </td>
          </tr>
          <tr>
        </table>
        <br>
        <table style="with: 60%" align="center">
        <caption style="font-size:15px"><b>Few more details to help people find this item easily! </b></caption>
           <tr>
            <td></td>
          </tr>
          <tr>
            <td>Food Type *</td>
            <td>
                <select class="form-control" name="foodList" id="tagPicker" multiple="multiple" placeholder="Search Here!" style="height:30px; width:215px">
          <c:forEach var="food" items="${foods}">
                  <option value="${food.id}">
                    <c:out value="${food.name}" />
                  </option>
                </c:forEach>
        </select>
            </td>
          </tr>
           <tr>
            <td></td>
          </tr>
           <tr>
            <td></td>
          </tr>
          <tr>
            <td>Category Type *</td>
            <td>
                <select class="form-control" name="categoryList" id="secondTagPicker" multiple="multiple" placeholder="Search Here!" style="height:30px; width:215px">
          <c:forEach var="category" items="${categories}">
                  <option value="${category.id}">
                    <c:out value="${category.name}" />
                  </option>
                </c:forEach>
            </td>
          </tr>
         </table>
         <br>
         <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning1">
         <i>All fields marked with * are required fields!</i>
         </p>
         <p id="warning2">
         <i>Please confirm all details before pressing <b>'Add Menu Item'</b> button!!</i>
         </p>
         </center>
          </tr>
          <tr>
            <td>
            <input type="reset" value="Reset" style="height:40px; width:135px;" />
            </td>
            <td>
            <input type="submit" value="Add Menu Item" style="height:40px; width:135px;"
                   onclick="return validatemapping(); return confirm('Are you sure? Press OK to Add Menu Item!')" />
            </td>
          </tr>
        </table>
         </div>
         </form:form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
    <div id="manage" class="exright" align="left" style="background-color:rgba(204, 204, 204, 0.8);">

      <form action="deleteMenuItem" method="post">
        <table style="with: 5%" align="right">
            <tr>
                <center>
                Update you <i>'menu items'</i> easily with few clicks!
                </center>
            </tr>
             <tr>
                <td>
                  <br>
                </td>
              </tr>
          <tr>
             <th> Menu Item Name </th>
             <th> Description  </th>
             <th> Price Per Unit </th>
             <th> Food Type </th>
             <th> Category </th>
             <th> Update Menu Item </th>
             <th> Delete Menu Item </th>
           </tr>
           <e:forEach var="menuItem" items="${restaurant.menuItems}">
             <tr>
               <td> <e:out value="${menuItem.name}" /> </td>
               <td> <e:out value="${menuItem.description}" /> </td>
               <td> <e:out value="${menuItem.price}" /> </td>
               </td>
               <td>
                 <d:forEach var="food" items="${menuItem.foods}">
                   <table style="with: 2%" align="center">
                     <tr>
                        <td>
                          <d:out value="${food.name}" /> </td>
                        </td>  
                     </tr>
                   </table>
                 </d:forEach>
               </td>
               <td>
                 <f:forEach var="category" items="${menuItem.categories}">
                   <table style="with: 2%" align="center">
                     <tr>
                        <td>
                          <f:out value="${category.name}" /> </td>
                        </td>  
                     </tr>
                   </table>
                 </f:forEach>
               </td>
               <td>
               <input type="button" value="Update" style="height:30px; width:120px"/>
               </td>
               <td>
               <input type="submit" value="Delete" style="height:30px; width:120px"
                      onclick="return confirm('Are you sure you want to delete?');"/>
               </td>
               <td>
               <input type="hidden" value="${menuItem.id}" name="menuItemId" />
               </td>
             </tr>
            <tr>
                <td>
                  <br>
                </td>
              </tr>
           </e:forEach>
       </table>
      </form>
     </div>
    <br><br>
  <script>
$(function(){
    $(".makeEditable").click(function(){
        $(':input').removeAttr("readonly");    
    });
    $(".makeNonEditable").click(function(){
        $(':input').attr("readonly", "readonly");    
    });               
});

//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){  
  /* Select2 plugin as tagpicker */
  $("#tagPicker").select2({
    closeOnSelect:false
  });

}); //script         
      

$(document).ready(function() {});

//Select2
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.min.js',function(){  
  /* Select2 plugin as tagpicker */
  $("#secondTagPicker").select2({
    closeOnSelect:false
  });

}); //script         
      

$(document).ready(function() {});


  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }

  function validatemapping() {
    var food = document.getElementById("tagPicker").value;
    var category = document.getElementById("secondTagPicker").value;
      if (food == '' || category == '') {
        alert('You must enter atleast one value for BOTH food and category!');
        return false;
      } else {
        return true;
      }
  }
  </script>
  </body>
</html>
