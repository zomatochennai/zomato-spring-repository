<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Classification Page</title>
    <script type = "text/javascript">
        function togglediv(id) {
            var div = document.getElementById(id);
            div.style.display = div.style.display == "none" ? "block" : "none";
        }
    </script>
    </head>
    <style>
body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    div.ex {
        text-align: right width:300px;
        padding: 10px;
        border: 5px solid grey;
        margin: 0px
    }
    div.outer {
        display: table;
        position: absolute;
        height: 100%;
        width: 100%;
    }

    div.middle {
        display: table-cell;
        vertical-align: middle;
    }

    div.inner {
        margin-left: auto;
        margin-right: auto; 
        width: 100%;
    }
    </style>
    <body>
            <form action="logout" method="post">
            <table style="with: 5%" align="right">
                  <tr>
                        <td><input type="submit" value="logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')" /></td>
                    </tr>
            </table>
            </form>
<div style="background-color:rgba(204, 204, 204, 0.7);">
        <h1>For Admin: Manage your Application!!</h1>
</div>
        <br><br>
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
                <table style="with: 50%">
                <h2>Add or delete Classifications here!!</h2>
                    <tr>
                        <input id="addclassification" type="button" value="Add new classification"
                         onclick="togglediv('mynewclassificationdiv')">
                        <div id="mynewclassificationdiv" style="display: none;">
                          <form action="addClassification" method="post">
                          Classification Name:
                            <input type="text" placeholder="New Classification name" id="classification name"
                             name="classificationName">
                            </input>
                          <input type="submit" value="Add Classification" 
                          onclick="return confirm('Are you sure? Press Ok to Continue!')"/>
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Classification Name</th>            
                    </tr>
                    <br>
                    <c:forEach var="classification" items="${classifications}">
                        <tr>
                            <td>
                            <input id="classificationName" type="text" name="classificationName" 
                            value="${classification.name}" maxlength="30"
                       readonly="readonly" required/>
                             </td>
                            <td> 
                          <form action="" method="post">
                        <a href="removeClassification?classificationId=${classification.id}" >
                        <input id="removeClassification" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                           </a>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
        

        <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td><a href="toAdminDashboard">
            <input id="discard" class="makeNonEditable" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </a>
            </td>
            <td>
            <input id="save" class="makeNonEditable" type="submit" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(true); return confirm('Are you sure? Press OK to continue')" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" class="makeEditable" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none';" />
         </center>
         </form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
      <br>
        
        
        
        
        
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
                <table style="with: 50%">
                <h2>Add or delete Categories here!!</h2>
                    <tr>
                        <input id="addcategory" type="button" value="Add new category"
                         onclick="togglediv('mynewcategorydiv')">
                        <div id="mynewcategorydiv" style="display: none;">
                          <form action="addCategory" method="post">
                          Category Name:
                            <input type="text" placeholder="New Category name" id="category name"
                             name="CategoryName"/>
                          <input type="submit" value="Add Category" 
                          onclick="return confirm('Are you sure? Press Ok to Continue!')"/>
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Category Name</th>            
                    </tr>
                    <br>
                    <c:forEach var="category" items="${categories}">
                        <tr>
                            <td><c:out value="${category.name}" /></td>
                            <td> 
                          <form action="" method="post">
                        <a href="removeCategory?categoryId=${category.id}" >
                        <input id="removeCategory" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                         </a>
                          </form>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
        </div>
        <br>
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
                <table style="with: 50%">
                <h2>Add or delete Foods here!!</h2>
                    <tr>
                        <input id="addfood" type="button" value="Add new Food"
                         onclick="togglediv('mynewfooddiv')">
                        <div id="mynewfooddiv" style="display: none;">
                          <form action="addFood" method="post">
                          Food Name:
                            <input type="text" placeholder="New Food name" id="food name"
                             name="foodName">
                            </input>
                          <input type="submit" value="Add Food" onclick="return confirm('Are you sure? Press Ok to Continue!')"/>
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Food Name</th>            
                    </tr>
                    <br>
                    <c:forEach var="food" items="${foods}">
                        <tr>
                            <td><c:out value="${food.name}" /></td>
                            <td> 
                          <form action="" method="post">
                        <a href="removeFood?foodId=${food.id}" >
                        <input id="removeFood" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                        </a>
                          </form>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
        </div>
    </body>
</html>
