<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Password Reset Page</title>
<script type="text/javascript">
  function checkForm(form) {

    if(form.newpassword.value != form.confirmpassword.value) {
        alert("New password(s) do not match!");
        return false;
    }
    else {
      return true;
    }
  }
  
  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }  <script for="window" event="onload">
      window.open('main.jsp',
      'Window',
      'width=800,height=600,resizable=0,status=0,toolbar=0');
      // Hack to bypass the confirm alert on window close.
      window.opener = window.top;
      window.open('', '_self', '');
      window.close();
  </script>
</script>
  </head>
  <style>
   body {
  background-image: url("view/media/cheesy-food-wallpaper.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body onLoad="return displayMessage()">
       <form:form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" /></td>
         </tr>
       </table>
    </form:form>
       <table style="with: 5%" align="left">
                <td>
                  <a href="goToHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px"/>
                  </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Password Reset Page </center></h1>
    </div>
    <br>
    <br>
    <div class="ex" style="background-color:rgba(264, 264, 264, 0.7);">
         <form action="changePassword" method="post" onsubmit="return checkForm(this);">
        <table style="with: 60%; font-size:17px" align="center">
        <tr>
          <center>
            Please fill all the fields!
          </center>
        </tr>
        <tr>
          <br>
        </tr>
        <tr>
          <td>
          Enter Old Password:
          </td>
          <td>
          <input id="oldpassword" type="password" name="oldPassword" required />
          </td>
        </tr>
        <tr>
          <td>
          Enter New Password:
          </td>
          <td>
          <input id="newpassword" type="password" name="newPassword" required />
          </td>
        </tr>
        <tr>
          <td>
          Confirm New Password:
          </td>
          <td>
          <input id="confirmpassword" type="password" name="confirmPassword" required />
          </td>
        </tr>
        </table>
        <br>
         <center>
         <input id="updatepassword" class="makeEditable" type="submit" value="Update Password" style="height:50px; width:150px;" />
         </center>
         <input id="mess" type="hidden" value="${message}" name="message" />
       </form>
     </div>
  </body>
</html>
