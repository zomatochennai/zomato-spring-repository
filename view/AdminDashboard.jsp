<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>     
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Manage Classification Page</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    
    <script>
    
    function check(input) {  
    if(input.validity.patternMismatch){  
        input.setCustomValidity("Please enter a valid input");  
    }  
    else {  
        input.setCustomValidity("");  
    }                 
    } 
  
    </script>
      
    <script>
    
    $(function(){
    $(".makeEditable").click(function(){
        $(':input').removeAttr("readonly");
    });
    $(".makeNonEditable").click(function(){
        $(':input').attr("readonly", "readonly");
    });               
})

  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }
    
        function togglediv(id) {
            var div = document.getElementById(id);
            div.style.display = div.style.display == "none" ? "block" : "none";
        }
    </script>
    </head>
    <style>
body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    div.ex {
        text-align: right width:30px;
        padding: 10px;
        border: 5px solid grey;
        margin: 0px
    }
    div.outer {
        display: table;
        position: absolute;
        height: 100%;
        width: 100%;
    }

    div.middle {
        display: table-cell;
        vertical-align: middle;
    }

    div.inner {
        margin-left: auto;
        margin-right: auto; 
        width: 100%;
    }
    div.exleft {
      text-align: left;
      float: left;
      display:inline-block;
      width:35%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    div.exright {
      text-align: right;
      float: right;
      display:inline-block;
      width:60%;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    </style>
    <body>
            <form action="logout" method="post">
            <table style="width: 5%" align="right">
                  <tr>
                        <td><input type="submit" value="logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')" /></td>
                    </tr>
            </table>
            </form>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="goToHome"> 
                    <input type="button" value="Go to Home" style="height:30px" >
                  </a>
                </td>
              </th>
       </table>
<div style="background-color:rgba(204, 204, 204, 0.7);">
        <h1><center> For Admin: Manage your Application!!</center></h1>
</div>
        <br><br>
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);" align="center">
                <table style="with: 50%">
                <h2>Add or delete Classifications here!!</h2>
                    <tr>
                        <div id="mynewclasssificationdiv">
                          <form action="addClassification" method="post" onsubmit="return confirm('Are you sure? Press OK to confirm!')">
                          <input type="hidden" value="redirect:/toAdminDashboard" name="targetLocation"/>
                            <input type="text" placeholder="New classification name" id="classification name"
                             name="classificationName" required pattern=".*\S+.*" oninput="check(this)"/>
                          <input type="submit" value="Add Classification" />
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Classification Name</th>            
                    </tr>
                    <br>
                    <form>
                    <c:forEach var="classification" items="${classifications}">
                        <tr>
                            <td><c:out value="${classification.name}" /></td>
                            <td> 
                          
                        <a href="removeClassification?classificationId=${classification.id}&targetLocation=redirect:/toAdminDashboard" >
                        <input id="removeClassification" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                        </a>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
  <!--      
        <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td><a href="toAdminDashboard">
            <input id="discard" class="makeNonEditable" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </a>
            </td>
            <td>
            <input id="save" class="makeNonEditable" type="submit" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none'; window.location.reload(true); return confirm('Are you sure? Press OK to continue')" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" class="makeEditable" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none';" />
         </center>-->
         </form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
      <br>
        
        
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);" align="center">
                <table style="with: 50%">
                <h2>Add or delete Categories here!!</h2>
                    <tr>
                        <div id="mynewcategorydiv">
                          <form action="addCategory" method="post" onsubmit="return confirm('Are you sure? Press OK to confirm!')">
                          <input type="hidden" value="redirect:/toAdminDashboard" name="targetLocation"/>
                            <input type="text" placeholder="New category name" id="category name"
                             name="categoryName" required pattern=".*\S+.*" oninput="check(this)"/>
                          <input type="submit" value="Add Category" />
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Category Name</th>            
                    </tr>
                    <br>
                    <c:forEach var="category" items="${categories}">
                        <tr>
                            <td><c:out value="${category.name}" /></td>
                            <td> 
                          <form>
                        <a href="removeCategory?categoryId=${category.id}&targetLocation=redirect:/toAdminDashboard" >
                        <input id="removeCategory" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                         </a>
                          </form>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
        </div>
        <br>
        <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);" align="center">
                <table style="with: 50%">
                <h2>Add or delete Foods here!!</h2>
                   <tr>
                        <div id="mynewfooddiv">
                          <form action="addFood" method="post" onsubmit="return confirm('Are you sure? Press OK to confirm!')">
                          <input type="hidden" value="redirect:/toAdminDashboard" name="targetLocation"/>
                            <input type="text" placeholder="New food name" id="food name"
                             name="foodName" required pattern=".*\S+.*" oninput="check(this)"/>
                          <input type="submit" value="Add Food" />
                          </form>
                          <br>
                        </div>                        
                    </tr>
                    <br>
                    <tr>
                        <th>Food Name</th>            
                    </tr>
                    <br>
                    <c:forEach var="food" items="${foods}">
                        <tr>
                            <td><c:out value="${food.name}" /></td>
                            <td> 
                          <form>
                        <a href="removeFood?foodId=${food.id}&targetLocation=redirect:/toAdminDashboard" >
                        <input id="removeFood" type="button" value="Delete" 
                               onclick="return confirm('Are you sure you want to Delete?')"/>
                        </a>
                          </form>
                         </td>
                       </tr>
                 </c:forEach>
              </table>
        </div>
       <input id="mess" type="hidden" value="${message}" name="message" />
    </body>
</html>
