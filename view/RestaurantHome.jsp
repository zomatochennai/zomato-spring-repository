<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ include file="scripts/VendorRoleCheck.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Home Page</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
    src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.4.min.js">
    </script>
    <script type="text/javascript">

  function displayMessage() {
    var message = document.getElementById("mess").value;
      if (message != '') {
        alert(message);
        return false;
      } else {
        return true;
      }
  }
    
    function check(input) {  
    if(input.validity.patternMismatch){  
        input.setCustomValidity("Please enter a valid input");  
    }  
    else {  
        input.setCustomValidity("");  
    }                 
    } 
  </script>
  </head>
  <style>
   body {
  background-image: url("view/media/all-food-basic-hd.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body onLoad="return displayMessage()">
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="redirectToPage?targetPage=RestaurantHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="manageRestaurant"> 
                    <input type="button" value="Manage Restaurant" style="height:30px; width:150px" />
                  </a>
                </td>
              </th> <!--
              <th>
                <td>
                  <a href="manageMenu"> 
                    <input type="button" value="Manage My Menu" style="height:30px; width:150px" />
                  </a>
                </td>
              </th> -->
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Restaurant Home Page</center></h1>
    </div>
    <br>
    <div id="top" style="background-color:rgba(204, 204, 204, 0.8);">
    <marquee behavior=alternate>
      <h4>Hello! ${userFirstName}, Welcome to your own online Food shop!</h4>
    </marquee>
    </div>
    <br>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
    <form method="post" action="updateUser">
        <table style="with: 60%" align="center">
          <tr>
          </tr>
          <tr>
            <td>First Name </td>
            <td>
                <input id="first" type="text" name="firstName" value="${onlineUser.firstName}" maxlength="30"
                       readonly required pattern=".*\S+.*" autocomplete="false" oninput="check(this)" />
            </td>
          </tr>
          <tr>
            <td>Last Name </td>
            <td>
                <input id="lastName" type="text" name="lastName" value="${onlineUser.lastName}" maxlength="30"
                       readonly required/>
            </td>
          </tr>
          <tr>
            <td>Mobile No. </td>
            <td>
                <input id="mobile" type="text" name="mobile" value="${onlineUser.mobile}" maxlength="10"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )"
                       readonly="readonly" required/>
            </td>
          </tr>
          <tr>
            <td>Email </td>
           <td>
               <input id="email" type="email" name="email" value="${onlineUser.email}" maxlength="30"
                      readonly="readonly" required/>
           </td>
           </tr>
         </table>
         <br>
         <div id="savedetails">
        <table style="with: 60%" align="center">
          <tr>
          <center>
         <p id="warning" style="display:none;">
         <i>Please confirm all details before pressing <b>'Save Changes'</b> button</i>
         </p>
         </center>
          </tr>
          <tr>
            <td><a href="redirectToPage?targetPage=RestaurantHome">
            <input id="discard" type="button" value="Discard Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('save').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </a>
            </td>
            <td>
            <input id="save" type="submit" value="Save Changes" style="height:40px; width:135px; display:none;" onclick="document.getElementById('warning').style.display = 'none'; document.getElementById('discard').style.display = 'none'; document.getElementById('update').style.display = 'block'; this.style.display = 'none';" />
            </td>
          </tr>
        </table>
         </div>
         <center>
         <input id="update" type="button" value="Edit Profile" style="height:40px; width:135px; display:block;"
          onclick="document.getElementById('discard').style.display = 'block'; document.getElementById('warning').style.display = 'block'; document.getElementById('save').style.display = 'block'; this.style.display = 'none'; document.getElementById('first').required = true; document.getElementById('first').removeAttribute('readonly');" />
         </center>
         </form>
         <input id="mess" type="hidden" value="${message}" name="message" />
      </div>
  </body>
</html>
