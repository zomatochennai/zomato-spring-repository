<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
  <%@ include file="scripts/CacheManagement.jsf" %>
    <%@ include file="scripts/LoggedInUserManagement.jsf" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration Successful</title>
  </head>
  <style>
body {
  background-image: url("view/media/food-pizza-hd-desktop-wallpaper.jpg");
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body>
    <div style="background-color:rgba(204, 204, 204, 0.3);">
    <h1><center>Registration Successful</center></h1>
    </div>
<table align="center">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png"alt="Idea! Let's Eat!" height="100" width="200">
        </a>
      </td>
    <tr>
    </table>
      <div class="ex" style="background-color:rgba(204, 204, 204, 0.2);">
          <table style="with: 50%" align="center">
            <tr>
              <td><b><i>
              <h2>
              <p class="tab blink" style="color:#0f061f;">
                 Registration is Successful! Let's Login to start your food journey!
              </p>
              <h2>
              </i> </b>
                <br>
                <center>
                <br><br> 
                <a href="redirectToPage?targetPage=LoginUser">
                  <input type="button" value="Go to Login" style="height:40px; width:120px"/> </a>
                </center>
              </td>
            </tr>
        </table>
    </div>
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>
  </body>
</html>
