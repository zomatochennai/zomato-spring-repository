<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Welcome Page</title>
  </head>
  <style type "text/css">
 body {
  background-image: url("view/media/food-error.png");
  background-repeat:no-repeat;
  background-size:cover;
  }
    .blink {
      -webkit-animation: blink .80s linear infinite;
      -moz-animation: blink .80s linear infinite;
      -ms-animation: blink .80s linear infinite;
      -o-animation: blink .80s linear infinite;
       animation: blink .80s linear infinite;
    }
    @-webkit-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-moz-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-ms-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @-o-keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    @keyframes blink {
      0% { opacity: 1; }
      50% { opacity: 1; }
      50.01% { opacity: 0; }
      100% { opacity: 0; }
    }
    div.ex {
      text-align: center width:300px;
      padding: 20px;
      border: 0px white;
      margin: 5px
    }
    img {
      opacity: 0.70;
      filter: alpha(opacity=70); 
    }
    }
    html, body {
	  margin:0;
	  padding:0;
	  height:100%;
    }
    #wrapper {
	  min-height:100%;
	  position:relative;
    }
    #footer {
	  background-color:rgba(204, 204, 204, 0.5);
	  width:100%;
	  height:25px;
	  position:absolute;
	  bottom:0;
	  left:0;
    }
  </style>
  <body bgcolor = "#DAA520">
    <div style="background-color:rgba(204, 204, 204, 0.3);">
    <marquee behavior=alternate><h1>Aw! Something is broken!</h1></marquee>
    </div>
    <br><br><br>
    <table align="left">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <br><br>
    <input type="button" value="Go back to Home!" style="height:40px; width:240px" />
        </a>
      </td>
    <tr>
    </table>
    <table align="right">
    <tr>
      <td>
        <a href = "redirectToPage?targetPage=Welcome">
        <img src="view/media/ideas2eat.png" alt="Idea! Let's Eat!" height="150" width="300">
        </a>
      </td>
    <tr>
    </table>
    <br><br><br>
    <br><br><br>
    <br><br><br>
    <br><br><br>
      <center>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.25);">
      <h2>
        <p class="tab blink" style="color:red;">
          We encountered an error while loading your request!
        </p>
      </h2>
      <h3>
        <p class="tab blink" style="color:red;">
          Relax! Our software engineers are working to fix this!
        </p>
       </h3>
    </div>
      </center> 
    <div id="footer"><center> Made with &#10084; in <strong>Namma Chennai</strong>!</center></div>     
  </body>
</html>
