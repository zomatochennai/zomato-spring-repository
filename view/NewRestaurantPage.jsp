<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ include file="scripts/CacheManagement.jsf" %>
<%@ include file="scripts/CookieManagement.jsf" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Restaurant Registration Page</title>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
$(document).ready(function(){
    $('input.timepicker').timepicker({
    timeFormat: 'hh:mm p',
    interval: 5,
    minTime: '00:00 am',
    maxTime: '23:59 pm',
    defaultTime: '',
    startTime: '00:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
    });
});
  </script>
  </head>
  <style>
   body {
  background-image: url("view/media/all-food-basic-hd.jpg");
  }
    div.ex {
      text-align: right width:300px;
      padding: 10px;
      border: 5px solid grey;
      margin: 0px
    }
  </style>
  <body>
       <form action="logout" method="post">
       <table style="with: 5%" align="right">
         <tr>
           <td><input type="submit" value="Logout" style="height:30px; width:100px" 
                      onclick="return confirm('Are you sure you want to Logout?')"/></td>
         </tr>
       </table>
    </form>
       <table style="with: 5%" align="right">
            <th>
                <td>
                  <a href="redirectToPage?targetPage=ChangePassword"> 
                    <input type="button" value="Change Password" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
            <th>
                <td>
                  <a href="redirectToPage?targetPage=RestaurantHome"> 
                    <input type="button" value="Go to Home Page" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
       <table style="with: 5%" align="left">
            <th>
                <td>
                  <a href="manageRestaurant"> 
                    <input type="button" value="Manage Restaurant" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
              <th>
                <td>
                  <a href="manageMenu"> 
                    <input type="button" value="Manage Menu Items" style="height:30px; width:150px" />
                  </a>
                </td>
              </th>
       </table>
    <div style="background-color:rgba(204, 204, 204, 0.7);">
    <h1><center> Restaurant Registration Page </center></h1>
    </div>
    <br>
    <br>
    <div class="ex" style="background-color:rgba(204, 204, 204, 0.7);">
      <form:form method="post" action="registerRestaurant" modelAttribute="restaurant"
            onsubmit="return ValidationEvent()">
        <table style="with: 60%" align="center">
            <caption style="font-size:20px"><b>Basic Restaurant Details:</b></caption>
          <tr><link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
            <td>Restaurant Name *</td>
            <td><form:form path="name"/>
                <input id="name" type="text" name="name" 
                      style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Restaurant Description *</td>
            <td><form:form path="description" />
                <input id="description" type="text" name="description" 
                style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Starting Time  *</td>
            <td><form:form path="startingTime"/>
                <input id="startingTime" class="timepicker text-center" jt-timepicker="" name="startingTime"
                      time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" 
                      time-format="model.options.timeFormat" start-time="model.options.startTime" min-time="model.options.minTime" 
                      max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" 
                      scrollbar="model.options.scrollbar" dropdown="model.options.dropdown" 
                      readonly="readonly" required />
            </td>
          </tr>
           <tr>
            <td>Closing Time  *</td>
            <td><form:form path="closingTime"/>
            <input id="closingTime" class="timepicker text-center" jt-timepicker="" name="closingTime"
                      time="model.time" time-string="model.timeString" default-time="model.options.defaultTime" 
                      time-format="model.options.timeFormat" start-time="model.options.startTime" min-time="model.options.minTime" 
                      max-time="model.options.maxTime" interval="model.options.interval" dynamic="model.options.dynamic" 
                      scrollbar="model.options.scrollbar" dropdown="model.options.dropdown"
                      readonly="readonly" required />
            </td>
          </tr>
           <tr>
            <td>Home Delivery *</td>
            <td><form:form path="homeDelivery"/>
                <input type="radio" name="homeDelivery" value="true" checked> Yes
                <input type="radio" name="homeDelivery" value="false"> No
            </td>
          </tr>
          <tr>
            <td>Restaurant Address Line 1 *</td>
            <td><form:form path="address.addressLineOne" />
                <input id="addressLineOne" type="text" name="addressLineOne"
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Restaurant Address Line 2</td>
            <td><form:form path="address.addressLineTwo" />
                <input id="addressLineTwo" type="text" name="addressLineTwo" 
                        style="height:15px; width:170px"/>
            </td>
          </tr>
          <tr>
            <td>City *</td>
            <td><form:form path="address.city"/>
                <input id="city" type="text" name="city" 
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>State *</td>
            <td><form:form path="address.state" />
                <input id="state" type="text" name="state"
                        style="height:15px; width:170px" required/>
            </td>
          </tr>
          <tr>
            <td>Pin Code *</td>
            <td><form:form path="address.pincode" />
                <input id="pincode" type="text" name="pincode"
                       onkeydown="return ( event.ctrlKey || event.altKey 
                         || (47<event.keyCode && event.keyCode<58 && event.shiftKey==false) 
                         || (95<event.keyCode && event.keyCode<106)
                         || (event.keyCode==8) || (event.keyCode==9) 
                         || (event.keyCode>34 && event.keyCode<40) 
                         || (event.keyCode==46) )" style="height:15px; width:170px" required/>
            </td>
         </table>
         <br>
         <i><center>Fields marked with * are required fields</center></i>
         <br>
         <center>
         <input type="reset" value="Reset" style="height:30px; width:120px" />
         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
         &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
         <input type="submit" value="Register" style="height:30px; width:120px" />
         </center>
       </form:form>
     </div>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
  </body>
</html>
